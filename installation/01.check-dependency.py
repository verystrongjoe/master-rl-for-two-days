from __future__ import absolute_import, division, print_function, unicode_literals

# Tensorflow and tf.keras
import tensorflow as tf

# Helper libraries
import numpy as np
import matplotlib

print('tensorflow version : {}'.format(tf.__version__))
print('matplotlib version : {}'.format(matplotlib.__version__))
print('numpy version : {}'.format(np.__version__))




