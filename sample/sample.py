import torch.nn as nn
import torch
import torch.nn.functional as F

class MyModel(nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.fc1 = nn.Linear(8, 8)
        self.fc2 = nn.Linear(12, 12)
        self.fc3 = nn.Linear(12, 20)

    def forward(self, x):
        # Use first part of x
        x1 = F.relu(self.fc1(x[:, :8]))
        # Concatenate the result of the first part with the second part of x
        x = torch.cat((x1, x[:, 8:]), dim=1)
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


model = MyModel()
x = torch.randn(1, 12)
output = model(x)