import torch
from torch.autograd import Variable as Variable

x = torch.Tensor(2,3)

x = Variable(torch.ones(2, 2) * 2, requires_grad=True)

z = 2 * (x * x) + 5 * x

print(z)

z.backward(torch.ones(2, 2))
print(x.grad)

print(z)

