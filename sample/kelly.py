"""
동전 던지기 게임(앞면/뒷면) = 0.5 이길 경우 2배 질 경우 0.5배로 베팅한 금액의 수익이 결정이 됨.
W = 2 L = 0.5 베팅금액을 P라고 했을때 베팅비율은 1, 0.9, 0.8, 0.7, 0.6, 0.5 로 놓고 게임 3회를 실행한다.

파산 금지, 복리 수익율을 적정한 수준에서 유지하자.

켈리의 기준 = 우위 / 배당률

우위 = (이길 경우 받는 총액 X 이길 확률  - 투입 금액) / 투입금액
동전 던지기(이길경우 2배, 질경우 1/2배)는 우위가 0이므로 (1배 배당이므로 분자가 0)

그러나 동전이 이상해서 55:45의 승률이라고 하면,
우위는 10% 우위로 나와서 10% 베팅하면 가장 빠른 복리 수익율 계산

켈리의 히스토리!!
https://steemit.com/kr/@pius.pius/7mhje6

"""
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

SEED_MONEY = 100  # dollar
BETTING_RATIO = 0.1  # betting ratio per each game
GOAL_RATIO = 5

IDX = 0
SEQ = 0

IDX_MAX = 100
SEQ_MAX = 100

def roll_dice(betting):
    p = np.random.rand() <= 0.5
    if p > 0.5:
        return betting * 2
    else:
        return betting * 0.5


if __name__ == '__main__':

    print('let\'s play game')

    i=0

    l_iter = []
    l_seq = []
    l_price = []

    while i < IDX_MAX:
        i = i + 1
        TRY_COUNT = 0
        CURRENT_MONEY = SEED_MONEY

        series = np.zeros(SEQ_MAX)
        # series[0] = CURRENT_MONEY

        l_iter.extend([i] * SEQ_MAX)
        l_seq.extend([s for s in range(SEQ_MAX)])
        SEQ = 0

        while CURRENT_MONEY > 0  and CURRENT_MONEY < SEED_MONEY * GOAL_RATIO and SEQ <SEQ_MAX:
            BETTING = CURRENT_MONEY * BETTING_RATIO
            CHANGES = CURRENT_MONEY * (1 - BETTING_RATIO)
            PROFIT = roll_dice(BETTING)
            CURRENT_MONEY = CHANGES + PROFIT
            TRY_COUNT = TRY_COUNT + 1
            # series[TRY_COUNT] = CURRENT_MONEY
            l_price.extend([CURRENT_MONEY])
            print('{} times : {}'.format(TRY_COUNT, CURRENT_MONEY))
            SEQ = SEQ + 1

        l_price.extend([CURRENT_MONEY] * (SEQ_MAX -SEQ))

    data = {'iteration': l_iter, 'sequence':l_seq, 'price':l_price}

    df = pd.DataFrame(data).groupby('sequence')['price'].agg({'Low Value':'min','High Value':'max','Mean':'mean'})
    df.reset_index(inplace=True)

    ax = df.plot(x='sequence', y='Mean', c='white')
    plt.fill_between(x='sequence', y1='Low Value', y2='High Value', data=df)

    print('ready...')

    plt.show()

