from torch.distributions.bernoulli import Bernoulli

b = Bernoulli(0.5)
for i in range(10):
    print(b.sample())