
class rainbow:
    epsilon_start = 1.0
    epsilon_final = 0.01
    epsilon_decay = 500

    beta_start = 0.4
    beta_frames = 100000

    gamma = 0.99
    batch_size = 32
    num_frames = 10000
    replay_memory_size = 100000
    lr = 0.0001

    hidden_nodes = 64