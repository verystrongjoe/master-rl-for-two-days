"""
referenced by
https://github.com/higgsfield/RL-Adventure/blob/master/4.prioritized%20dqn.ipynb

"""

import math, random
import gym
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.autograd as autograd
import torch.nn.functional as F
import matplotlib.pyplot as plt
import day2.argslist as args
from common.utils import *

CURRENT_FILE_NAME, CURRENT_FILE_PATH, FILE_NAME_FOR_LOG = get_additional_log_info(__file__)

USE_CUDA = torch.cuda.is_available()
Variable = lambda *args, **kwargs : autograd.Variable(*args, **kwargs).cuda() if USE_CUDA else autograd.Variable(*args, **kwargs)


class NaivePrioritizedBuffer(object):
    def __init__(self, capacity, prob_alpha=0.6):
        self.prob_alpha = prob_alpha
        self.capacity = capacity
        self.buffer = []
        self.pos = 0
        self.priorities = np.zeros((capacity,), dtype=np.float32)

    def push(self, state, action, reward, next_state, done):
        assert state.ndim == next_state.ndim
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        max_prio = self.priorities.max() if self.buffer else 1.0

        # 리플레이 버퍼가 max 보다 작으면 걍 append
        if len(self.buffer) < self.capacity:
            self.buffer.append((state, action, reward, next_state, done))
        else:  # pos가 1씩 증가하면서 transition sample을 저장
            self.buffer[self.pos] = (state, action, reward, next_state, done)

        self.priorities[self.pos] = max_prio  # 각 샘플의 priorities를 저장하는 부분임 버퍼가 비었을땐 1.0이고 아닌경우는 기존의 priorities의 max값 !!
        self.pos = (self.pos + 1) % self.capacity

    def sample(self, batch_size, beta=0.4):
        if len(self.buffer) == self.capacity: # 풀일때
            prios = self.priorities
        else: # 풀이지 않을때는 현재 차있는 데이터에 대해서만,
            prios = self.priorities[:self.pos]

        probs = prios ** self.prob_alpha
        probs /= probs.sum()

        indices = np.random.choice(len(self.buffer), batch_size, p=probs)
        samples = [self.buffer[idx] for idx in indices]

        # Importance Sampling weights
        total = len(self.buffer)
        weights = (total * probs[indices]) ** (-beta) # 위에서 뽑힌 샘플 배치에 대해서 각 확율과 전체 개수를 곱하고 -beta만큼의 승 처리
        weights /= weights.max()  # 노말라이즈!!
        weights = np.array(weights, dtype=np.float32)

        # 샘플링된 transition samples들을 아래와 같이 rows 별로 grouping이 가능!!
        batch = list(zip(*samples))
        states = np.concatenate(batch[0])
        actions = batch[1]
        rewards = batch[2]
        next_states = np.concatenate(batch[3])
        dones = batch[4]

        return states, actions, rewards, next_states, dones, indices, weights

    def update_priorities(self, batch_indices, batch_priorities):
        for idx, prio in zip(batch_indices, batch_priorities):
            self.priorities[idx] = prio

    def __len__(self):
        return len(self.buffer)


beta_start = 0.4
beta_frames = 1000
beta_by_frame = lambda frame_idx : min(1.0, beta_start + frame_idx * (1.0 -beta_start) / beta_frames)

# plt.plot([beta_by_frame(i) for i in range(10000)])


env_name = 'CartPole-v0'
env = gym.make(env_name)

epsilon_by_frame = lambda frame_idx : args.rainbow.epsilon_final + (args.rainbow.epsilon_start - args.rainbow.epsilon_final) * math.exp(-1. * frame_idx / args.rainbow.epsilon_decay)

plt.plot([epsilon_by_frame(i) for i in range(10000)])
plt.show()


class DQN(nn.Module):

    def __init__(self, num_inputs, num_actions):
        super(DQN, self).__init__()

        self.layers = nn.Sequential(
            nn.Linear(env.observation_space.shape[0], 128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.ReLU(),
            nn.Linear(128, env.action_space.n)
        )

    def forward(self, x):
        return self.layers(x)

    def act(self, state, epsilon):
        if random.random() > epsilon:
            state = Variable(torch.FloatTensor(state).unsqueeze(0), volatile=True)
            q_value = self.forward(state)
            action = q_value.max(1)[1].data[0]
            action = action.item()
        else:
            action = random.randrange(env.action_space.n)
        return action


online_model = DQN(env.observation_space.shape, env.action_space.n)
target_model = DQN(env.observation_space.shape, env.action_space.n)

if USE_CUDA:
    online_model = online_model.cuda()
    target_model = target_model.cuda()

optimizer = optim.Adam(online_model.parameters())

replay_buffer = NaivePrioritizedBuffer(100000)


# current policy network and target network 동기화 하는 함수
def update_target(online_model, target_model):
    target_model.load_state_dict(online_model.state_dict())
    

update_target(online_model, target_model)


# TD에러 계산하는 부분 심플
def compute_td_loss(batch_size, beta):
    state, action, reward, next_state, done, indices, weights = replay_buffer.sample(batch_size, beta)

    state = Variable(torch.FloatTensor(np.float32(state)))
    next_state = Variable(torch.FloatTensor(np.float32(next_state)))
    action = Variable(torch.LongTensor(action))
    reward     = Variable(torch.FloatTensor(reward))
    done       = Variable(torch.FloatTensor(done))
    weights    = Variable(torch.FloatTensor(weights))

    q_values = online_model(state)
    next_q_values = target_model(next_state)

    q_value = q_values.gather(1, action.unsqueeze(1)).squeeze(1)
    next_q_value = next_q_values.max(1)[0]
    expected_q_value = reward + args.rainbow.gamma * next_q_value * (1 - done)

    loss = (q_value - expected_q_value.detach()).pow(2) * weights
    prios = loss + 1e-5
    loss = loss.mean()

    optimizer.zero_grad()
    loss.backward()
    replay_buffer.update_priorities(indices, prios.data.cpu().numpy())
    optimizer.step()

    return loss


# Training
losses = []
all_rewards = []
episode_reward = 0


state = env.reset()


for frame_idx in range(1, args.rainbow.num_frames + 1):
    epsilon = epsilon_by_frame(frame_idx)
    action = online_model.act(state, epsilon)
    next_state, reward, done, _ = env.step(action)
    replay_buffer.push(state, action, reward, next_state, done)

    state = next_state
    episode_reward += reward

    if done:
        state = env.reset()
        all_rewards.append(episode_reward)
        episode_reward = 0

    if len(replay_buffer) > args.rainbow.batch_size:
        beta = beta_by_frame(frame_idx)
        loss = compute_td_loss(args.rainbow.batch_size, beta)
        losses.append(loss.data.item())  # debug로 찍어보기 바람!!

    if frame_idx % 1000 == 0:
        update_target(online_model, target_model)

plot(all_rewards, losses, FILE_NAME_FOR_LOG)