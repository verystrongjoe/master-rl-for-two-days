import math, random
import numpy as np
import gym

# pytorch
import torch
import torch.nn as nn
import torch.optim as optim
import torch.autograd as autograd
import torch.nn.functional as F

# parameter and runner settings
import day2.argslist as args
import argparse
from common.utils import *
import os

"""
define parser
"""
parser = argparse.ArgumentParser(description='Configuration')

parser.add_argument(OPS.BATCH_SIZE.value, type=int, default=32, help="batch size")
parser.add_argument(OPS.REPLAY_MEMORY_SIZE.value, type=int, default=100000, help="replay memory size")
parser.add_argument(OPS.LEARNING_RATE.value, type=float, default=0.001, help="learning rate")
parser.add_argument(OPS.N_STEPS.value, type=int, default=1500000, help="n steps for training")
parser.add_argument(OPS.ENV_NAME.value, type=str, default='PongNoFrameskip-v4', help="env name")
parser.add_argument(OPS.DISCOUNT_FACTOR.value, type=float, default=0.99, help="discount factor")
parser.add_argument(OPS.BETA_FRAMES.value, type=float, default=100000, help="discount factor")
parser.add_argument(OPS.BETA_START.value, type=float, default=0.4, help="discount factor")
parser.add_argument(OPS.EPSILON_START.value, type=float, default=1.0, help="discount factor")
parser.add_argument(OPS.EPSILON_FINAL.value, type=float, default=0.01, help="discount factor")
parser.add_argument(OPS.EPSILON_DECAY.value, type=float, default=30000, help="discount factor")


p_args = parser.parse_args()
dict_args = vars(p_args)
post_fix = create_post_fix(dict_args)

CURRENT_FILE_NAME = os.path.basename(__file__).split('.')[0]
CURRENT_FILE_PATH = os.path.sep.join(os.path.abspath(__file__).split(os.path.sep)[:-1])
FILE_NAME_FOR_LOG = os.path.basename(__file__).split('.')[0] + "_" + yyyymmdd24hhmmss() + post_fix

env_id = dict_args[OPS.ENV_NAME()]
args.batch_size = dict_args[OPS.BATCH_SIZE()]
args.gamma = dict_args[OPS.DISCOUNT_FACTOR()]
args.num_frames = dict_args[OPS.N_STEPS()]
args.replay_memory_size = dict_args[OPS.REPLAY_MEMORY_SIZE()]
args.lr = dict_args[OPS.LEARNING_RATE()]

args.epsilon_start = dict_args[OPS.EPSILON_START()]
args.epsilon_final = dict_args[OPS.EPSILON_FINAL()]
args.epsilon_decay = dict_args[OPS.EPSILON_DECAY()]


USE_CUDA = torch.cuda.is_available()
Variable = lambda *args, **kwargs : autograd.Variable(*args, **kwargs).cuda() if USE_CUDA else autograd.Variable(*args, **kwargs)

from common.wrappers import make_atari, wrap_deepmind, wrap_pytorch


class NaivePrioritizedBuffer(object):
    def __init__(self, capacity, prob_alpha=0.6):
        self.prob_alpha = prob_alpha
        self.capacity = capacity
        self.buffer = []
        self.pos = 0
        self.priorities = np.zeros((capacity,), dtype=np.float32)

    def push(self, state, action, reward, next_state, done):
        assert state.ndim == next_state.ndim
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        max_prio = self.priorities.max() if self.buffer else 1.0

        if len(self.buffer) < self.capacity:
            self.buffer.append((state, action, reward, next_state, done))
        else:
            self.buffer[self.pos] = (state, action, reward, next_state, done)

        self.priorities[self.pos] = max_prio
        self.pos = (self.pos + 1) % self.capacity

    def sample(self, batch_size, beta=0.4):
        if len(self.buffer) == self.capacity:
            prios = self.priorities
        else:
            prios = self.priorities[:self.pos]

        probs = prios ** self.prob_alpha
        probs /= probs.sum()

        indices = np.random.choice(len(self.buffer), batch_size, p=probs)
        samples = [self.buffer[idx] for idx in indices]

        total = len(self.buffer)
        weights = (total * probs[indices]) ** (-beta)
        weights /= weights.max()
        weights = np.array(weights, dtype=np.float32)

        batch = zip(*samples)
        states = np.concatenate(batch[0])
        actions = batch[1]
        rewards = batch[2]
        next_states = np.concatenate(batch[3])
        dones = batch[4]

        return states, actions, rewards, next_states, dones, indices, weights

    def update_priorities(self, batch_indices, batch_priorities):
        for idx, prio in zip(batch_indices, batch_priorities):
            self.priorities[idx] = prio

    def __len__(self):
        return len(self.buffer)


env = make_atari(env_id)
env = wrap_deepmind(env)
env = wrap_pytorch(env)


class CnnDQN(nn.Module):
    def __init__(self, input_shape, num_actions):
        super(CnnDQN, self).__init__()

        self.input_shape = input_shape
        self.num_actions = num_actions

        self.features = nn.Sequential(
            nn.Conv2d(input_shape[0], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )

        self.fc = nn.Sequential(
            nn.Linear(self.feature_size(), 512),
            nn.ReLU(),
            nn.Linear(512, self.num_actions)
        )

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

    def feature_size(self):
        return self.features(autograd.Variable(torch.zeros(1, *self.input_shape))).view(1,-1).size(1)

    def act(self, state, epsilon):
        if random.random() > epsilon:
            state = Variable(torch.FloatTensor(np.float32(state)).unsqueeze(0), volatile=True)
            q_value = self.forward(state)
            action = q_value.max(1)[1].item()  ## 디버깅으로 찍어보기 바람!!
        else:
            action = random.randrange(env.action_space.n)
        return action

# TD에러 계산하는 부분 심플
def compute_td_loss(batch_size, beta):
    state, action, reward, next_state, done, indices, weights = replay_buffer.sample(batch_size, beta)

    state = Variable(torch.FloatTensor(np.float32(state)))
    next_state = Variable(torch.FloatTensor(np.float32(next_state)))
    action = Variable(torch.LongTensor(action))
    reward     = Variable(torch.FloatTensor(reward))
    done       = Variable(torch.FloatTensor(done))
    weights    = Variable(torch.FloatTensor(weights))

    q_values = online_model(state)
    next_q_values = target_model(next_state)

    q_value = q_values.gather(1, action.unsqueeze(1)).squeeze(1)
    next_q_value = next_q_values.max(1)[0]
    expected_q_value = reward + args.gamma * next_q_value * (1 - done)

    loss = (q_value - expected_q_value.detach()).pow(2) * weights
    prios = loss + 1e-5
    loss = loss.mean()

    optimizer.zero_grad()
    loss.backward()
    replay_buffer.update_priorities(indices, prios.data.cpu().numpy())
    optimizer.step()

    return loss

# current policy network and target network 동기화 하는 함수
def update_target(online_model, target_model):
    target_model.load_state_dict(online_model.state_dict())


online_model = CnnDQN(env.observation_space.shape, env.action_space.n)
target_model = CnnDQN(env.observation_space.shape, env.action_space.n)

if USE_CUDA:
    online_model = online_model.cuda()
    target_model = target_model.cuda()

optimizer = optim.Adam(online_model.parameters(), lr=args.lr)

replay_buffer = NaivePrioritizedBuffer(args.replay_memory_size)

update_target(online_model, target_model)

epsilon_by_frame = lambda frame_idx: args.epsilon_final + (args.epsilon_start - args.epsilon_final) * math.exp(-1. * frame_idx / args.epsilon_decay)
# plt.plot([epsilon_by_frame(i) for i in range(args.num_frames)])

# Beta Prioritized Experience Replay
beta_by_frame = lambda frame_idx: min(1.0, args.beta_start + frame_idx * (1.0 - args.beta_start) / args.beta_frames)
# plt.plot([beta_by_frame(i) for i in range(args.num_frames)])


losses = []
all_rewards = []
episode_reward = 0

state = env.reset()
for frame_idx in range(1, args.num_frames + 1):
    epsilon = epsilon_by_frame(frame_idx)
    action = online_model.act(state, epsilon)

    next_state, reward, done, _ = env.step(action)
    replay_buffer.push(state, action, reward, next_state, done)

    state = next_state
    episode_reward += reward

    if done:
        state = env.reset()
        all_rewards.append(episode_reward)
        episode_reward = 0

    if len(replay_buffer) > args.replay_memory_size:
        beta = beta_by_frame(frame_idx)
        loss = compute_td_loss(args.batch_size, beta)

        # losses.append(loss.data[0])
        losses.append(loss.data.item())

    if frame_idx % 10000 == 0:
        print('frame {} is passing..'.format(frame_idx))

    if frame_idx % 1000 == 0:
        update_target(online_model, target_model)

plot(all_rewards, losses, FILE_NAME_FOR_LOG)
