import math, random
import numpy as np
import gym

# pytorch
import torch
import torch.nn as nn
import torch.optim as optim
import torch.autograd as autograd
import torch.nn.functional as F

# parameter and runner settings
import day2.argslist as args
import argparse
from common.utils import *
from common.replay_buffer import *
import os

os.environ["CUDA_VISIBLE_DEVICES"]="1"

from common.wrappers import make_atari, wrap_deepmind, wrap_pytorch

"""
define parser
"""
parser = argparse.ArgumentParser(description='Configuration')

parser.add_argument(OPS.BATCH_SIZE.value, type=int, default=32, help="batch size")
parser.add_argument(OPS.REPLAY_MEMORY_SIZE.value, type=int, default=100000, help="replay memory size")
parser.add_argument(OPS.LEARNING_RATE.value, type=float, default=0.001, help="learning rate")
parser.add_argument(OPS.N_STEPS.value, type=int, default=1500000, help="n steps for training")
parser.add_argument(OPS.ENV_NAME.value, type=str, default='PongNoFrameskip-v4', help="env name")
parser.add_argument(OPS.DISCOUNT_FACTOR.value, type=float, default=0.99, help="discount factor")
parser.add_argument(OPS.BETA_FRAMES.value, type=float, default=100000, help="discount factor")
parser.add_argument(OPS.BETA_START.value, type=float, default=0.4, help="discount factor")



p_args = parser.parse_args()
dict_args = vars(p_args)
post_fix = create_post_fix(dict_args)

CURRENT_FILE_NAME = os.path.basename(__file__).split('.')[0]
CURRENT_FILE_PATH = os.path.sep.join(os.path.abspath(__file__).split(os.path.sep)[:-1])
FILE_NAME_FOR_LOG = os.path.basename(__file__).split('.')[0] + "_" + yyyymmdd24hhmmss() + post_fix

env_id = dict_args[OPS.ENV_NAME()]
args.batch_size = dict_args[OPS.BATCH_SIZE()]
args.gamma = dict_args[OPS.DISCOUNT_FACTOR()]
args.num_frames = dict_args[OPS.N_STEPS()]
args.replay_memory_size = dict_args[OPS.REPLAY_MEMORY_SIZE()]
args.lr = dict_args[OPS.LEARNING_RATE()]


USE_CUDA = torch.cuda.is_available()
Variable = lambda *args, **kwargs : autograd.Variable(*args, **kwargs).cuda() if USE_CUDA else autograd.Variable(*args, **kwargs)


env = make_atari(env_id)
env = wrap_deepmind(env)
env = wrap_pytorch(env)

class NoisyLinear(nn.Module):

    def __init__(self, in_features, out_features, std_init=0.4):
        super(NoisyLinear, self).__init__()

        self.in_features = in_features
        self.out_features = out_features
        self.std_init = std_init

        self.weight_mu = nn.Parameter(torch.FloatTensor(out_features, in_features))
        self.weight_sigma = nn.Parameter(torch.FloatTensor(out_features, in_features))
        self.register_buffer('weight_epsilon', torch.FloatTensor(out_features, in_features))

        self.bias_mu = nn.Parameter(torch.FloatTensor(out_features))
        self.bias_sigma = nn.Parameter(torch.FloatTensor(out_features))
        self.register_buffer('bias_epsilon', torch.FloatTensor(out_features))

        self.reset_parameters()
        self.reset_noise()

    def forward(self, x):
        if self.training:
            weight = self.weight_mu + self.weight_sigma.mul(Variable(self.weight_epsilon))
            bias = self.bias_mu + self.bias_sigma.mul(Variable(self.bias_epsilon))
        else:
            weight = self.weight_mu
            bias = self.bias_mu

        return F.linear(x, weight, bias)

    def reset_parameters(self):
        mu_range = 1 / math.sqrt(self.weight_mu.size(1))

        self.weight_mu.data.uniform_(-mu_range, mu_range)
        self.weight_sigma.data.fill_(self.std_init / math.sqrt(self.weight_sigma.size(1))) # in_feature size

        self.bias_mu.data.uniform_(-mu_range, mu_range)
        self.bias_sigma.data.fill_(self.std_init / math.sqrt(self.bias_sigma.size(0))) # out_features size

    def reset_noise(self):
        epsilon_in = self._scale_noise(self.in_features)
        epsilon_out = self._scale_noise(self.out_features)

        self.weight_epsilon.copy_(epsilon_out.ger(epsilon_in))
        self.bias_epsilon.copy_(self._scale_noise(self.out_features))

    def _scale_noise(self, size):
        x = torch.randn(size)
        x = x.sign().mul(x.abs().sqrt())  # -1~1 ㅅ
        return x


class NoisyCnnDQN(nn.Module):
    def __init__(self, input_shape, num_actions):
        super(NoisyCnnDQN, self).__init__()

        self.input_shape = input_shape
        self.num_actions = num_actions

        self.features = nn.Sequential(
            nn.Conv2d(input_shape[0], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )

        self.noisy1 = NoisyLinear(self.feature_size(), 512)
        self.noisy2 = NoisyLinear(512, env.action_space.n)

    def forward(self, x):
        batch_size = x.size(0)

        x = x / 255.
        x = self.features(x)
        x = x.view(batch_size, -1)

        x = F.relu(self.noisy1(x))
        x = self.noisy2(x)
        return x

    def reset_noise(self):
        self.noisy1.reset_noise()
        self.noisy2.reset_noise()

    def feature_size(self):
        return self.features(autograd.Variable(torch.zeros(1, *self.input_shape))).view(1, -1).size(1)

    def act(self, state):
        state = Variable(torch.FloatTensor(np.float32(state)).unsqueeze(0), volatile=True)
        q_value = self.forward(state)
        action = q_value.max(1)[1].data[0]
        return action.item()

online_model = NoisyCnnDQN(env.observation_space.shape, env.action_space.n)
target_model = NoisyCnnDQN(env.observation_space.shape, env.action_space.n)

if USE_CUDA:
    online_model = online_model.cuda()
    target_model = target_model.cuda()

optimizer = optim.Adam(online_model.parameters(), lr=args.lr)

beta_by_frame = lambda frame_idx : min(1.0, args.beta_start + frame_idx * (1.0 - args.beta_start) / args.beta_frames)
replay_buffer = PrioritizedReplayBuffer(args.replay_memory_size, alpha=0.6)

def update_target(current_model, target_model):
    target_model.load_state_dict(current_model.state_dict())


update_target(online_model, target_model)


def compute_td_loss(batch_size, beta):
    state, action, reward, next_state, done, weights, indices = replay_buffer.sample(batch_size, beta)

    state = Variable(torch.FloatTensor(np.float32(state)))
    next_state = Variable(torch.FloatTensor(np.float32(next_state)))
    action = Variable(torch.LongTensor(action))
    reward = Variable(torch.FloatTensor(reward))
    done = Variable(torch.FloatTensor(np.float32(done)))
    weights = Variable(torch.FloatTensor(weights))

    q_values = online_model(state)
    next_q_values = target_model(next_state)

    q_value = q_values.gather(1, action.unsqueeze(1)).squeeze(1)
    next_q_value = next_q_values.max(1)[0]
    expected_q_value = reward + args.gamma * next_q_value * (1 - done)

    loss = (q_value - expected_q_value.detach()).pow(2) * weights
    prios = loss + 1e-5
    loss = loss.mean()

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    replay_buffer.update_priorities(indices, prios.data.cpu().numpy())
    online_model.reset_noise()
    target_model.reset_noise()

    return loss

losses = []
all_rewards = []
episode_reward = 0

state = env.reset()
for frame_idx in range(1, args.num_frames + 1):
    action = online_model.act(state)

    next_state, reward, done, _ = env.step(action)
    replay_buffer.push(state, action, reward, next_state, done)

    state = next_state
    episode_reward += reward

    if done:
        state = env.reset()
        all_rewards.append(episode_reward)
        episode_reward = 0

    if len(replay_buffer) > args.batch_size:
        beta = beta_by_frame(frame_idx)
        loss = compute_td_loss(args.batch_size, beta)
        losses.append(loss.item())

    if frame_idx % 10000 == 0:
        print('{} frame_idx is passing'.format(frame_idx))

    if frame_idx % 1000 == 0:
        update_target(online_model, target_model)

plot(all_rewards, losses, FILE_NAME_FOR_LOG)