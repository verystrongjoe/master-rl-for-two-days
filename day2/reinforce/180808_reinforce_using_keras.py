import os, sys
wd = os.getcwd()
print('changed to working directory. Now working dir : {}'.format(wd))
os.chdir(wd)

import gym
import numpy as np
import pylab
import timeit

from keras.layers import Dense, Input
from keras.models import Model
from keras.optimizers import Adam


EPISODES = 10000
env = gym.make('CartPole-v0')

is_training = True


class REINFORCEAgent:
    def __init__(self, env, render=False, learning_rate=0.001, discount_factor=0.99):
        self.state_size = env.observation_space.shape[0]
        self.action_size = env.action_space.n
        self.render = render
        self.discounted_factor = discount_factor
        self.learning_rate = learning_rate
        self.model = self.build_network()
        self.states, self.actions, self.rewards = [], [], []

    def build_network(self):
        i = Input([self.state_size])
        h1 = Dense(24, activation='relu', kernel_initializer='glorot_uniform')(i)
        h2 = Dense(24, activation='relu', kernel_initializer='glorot_uniform')(h1)
        o = Dense(self.action_size, activation='softmax', kernel_initializer='glorot_uniform')(h2)
        policy_network = Model(i, o)
        policy_network.summary()
        policy_network.compile(loss='categorical_crossentropy', optimizer=Adam(lr=self.learning_rate))
        return policy_network

    def get_action(self, state):
        policy = self.model.predict(state,batch_size=1).flatten()
        return np.random.choice(self.action_size, 1, p=policy)[0]

    def discount_rewards(self,rewards):
        gt = np.zeros_like(rewards)
        r = 0
        for i in reversed(range(len(rewards))):
            r = r * self.discounted_factor + rewards[i]
            gt[i] = r
        return gt

    def append_sample(self, state, action, reward):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)

    def train_model(self):
        episode_length = len(self.states)

        discounted_rewards = self.discount_rewards(self.rewards)
        discounted_rewards -= np.mean(discounted_rewards)
        discounted_rewards /= np.std(discounted_rewards)

        update_inputs = np.zeros((episode_length, self.state_size))

        # if it use zeros_like, we got error TypeError: 'numpy.int32' object does not support item assignment
        Gt = np.zeros((episode_length, self.action_size))

        for i in range(episode_length):
            update_inputs[i] = self.states[i]
            Gt[i][self.actions[i]] = discounted_rewards[i]

        self.model.fit(update_inputs, Gt, epochs=1, verbose=0)
        self.states, self.actions, self.rewards = [], [], []

if __name__ == '__main__':

    agent = REINFORCEAgent(env, render=True)
    scores, episodes = [], []

    # if os.path.exists("./save_model/cartpole_reinforce.h5"):
    #     print('load existing model')
    #     agent.model.load_weights("./save_model/cartpole_reinforce.h5")
    #     print('completed to load')

    start_time = timeit.default_timer()
    current_time = timeit.default_timer()

    for e in range(EPISODES):
        score = 0
        o = env.reset()
        o = np.reshape(o, [1, agent.state_size])

        while True:
            if agent.render:
                env.render()

            a = agent.get_action(o)
            next_o, r, d, _ = env.step(a)
            next_o = np.reshape(next_o, [1, agent.state_size])

            if is_training:
                r = r if not d or score == 199 else 0
                agent.append_sample(o, a, r)
                score += r
                o = next_o

            if d:
                if is_training:
                    agent.train_model()

                score = score if score == 200 else score + 100
                scores.append(score)
                episodes.append(e)


                if is_training:
                    # exit
                    if np.mean(scores[-min(10, len(scores)):]) > 195:
                        pylab.plot(episodes, scores, 'b')
                        pylab.savefig("./save_graph/cartpole_reinforce.png")
                        sys.exit()
                    break

        if is_training:
            # save the model
            if e % 200 == 0:
                pylab.plot(episodes, scores, 'b')
                pylab.savefig("./save_graph/cartpole_reinforce.png")

                agent.model.save_weights("./save_model/cartpole_reinforce.h5")
                current_time = timeit.default_timer()
                print('executed time for running 200 episodes : {}'.format(current_time-start_time))
                start_time = current_time
