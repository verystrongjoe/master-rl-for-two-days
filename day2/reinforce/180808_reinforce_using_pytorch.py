import os
import sys

wd = os.getcwd()
print('changed to working directory. Now working dir : {}'.format(wd))
os.chdir(wd)

import gym
import random
import numpy as np
import pylab
import timeit

# from keras.layers import Dense, Input
# from keras.models import Model
# from keras.optimizers import Adam

import torch
import torch.nn as nn
import torch.nn.functional as f
import torch.optim as optim
from torch.distributions import Categorical


EPISODES = 10000
env = gym.make('CartPole-v1')
is_training = True
env.seed(543)
torch.manual_seed(543)

class Policy(nn.Module):
    def __init__(self, state_size, action_size):
        super(Policy, self).__init__()
        self.affine1 = nn.Linear(state_size, 128)
        self.affine2 = nn.Linear(128, action_size)
        self.saved_log_probs = []
        self.rewards = []

    def forward(self, state):
        x = f.relu(self.affine1(state))
        action_scores = self.affine2(x)
        return f.softmax(action_scores, dim=1)

class REINFORCEAgent:
    def __init__(self, env, render=False, learning_rate=0.001, discount_factor=0.99):
        self.state_size = env.observation_space.shape[0]
        self.action_size = env.action_space.n
        self.render = False
        self.discounted_factor = discount_factor
        self.learning_rate = learning_rate
        self.policy, self.optim, self.eps = self.build_network()

        self.states, self.actions, self.rewards = [], [], []

    def build_network(self):
        # i = Input([self.state_size])
        # h1 = Dense(24, activation='relu', kernel_initializer='glorot_uniform')(i)
        # h2 = Dense(24, activation='relu', kernel_initializer='glorot_uniform')(h1)
        # o = Dense(self.action_size, activation='softmax', kernel_initializer='glorot_uniform')(h2)
        # policy_network = Model(i, o)
        # policy_network.summary()
        # policy_network.compile(loss='categorical_crossentropy', optimizer=Adam(lr=self.learning_rate))
        # return policy_network
        policy = Policy(self.state_size,self.action_size)
        optimizer = optim.Adam(policy.parameters(), lr=1e-2)
        eps = np.finfo(np.float32).eps.item()
        return policy, optimizer, eps

    def get_action(self, state):
        # policy = self.model.predict(state,batch_size=1).flatten()
        # return np.random.choice(self.action_size, 1, p=policy)[0]
        state = torch.from_numpy(state).float().unsqueeze(0)
        probs = self.policy(state)
        m = Categorical(probs)
        action = m.sample()
        self.policy.saved_log_probs.append(m.log_prob(action))
        return action.item()

    # def discount_rewards(self,rewards):
    #     gt = np.zeros_like(rewards)
    #     r = 0
    #     for i in reversed(range(len(rewards))):
    #         r = r * self.discounted_factor + rewards[i]
    #         gt[i] = r
    #     return gt

    def append_sample(self, state, action, reward):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)

    def train_model(self):
        R = 0
        policy_loss = []
        rewards = []
        for r in self.policy.rewards[::-1]:
            R = r + 0.99 * R
            rewards.insert(0, R)
        rewards = torch.tensor(rewards)
        rewards = (rewards - rewards.mean()) / (rewards.std() + self.eps)
        for log_prob, reward in zip(self.policy.saved_log_probs, rewards):
            policy_loss.append(-log_prob * reward)
        self.optim.zero_grad()
        policy_loss = torch.cat(policy_loss).sum()
        policy_loss.backward()
        self.optim.step()
        del self.policy.rewards[:]
        del self.policy.saved_log_probs[:]

        # episode_length = len(self.states)
        #
        # discounted_rewards = self.discount_rewards(self.rewards)
        # discounted_rewards -= np.mean(discounted_rewards)
        # discounted_rewards /= np.std(discounted_rewards)
        #
        # update_inputs = np.zeros((episode_length, self.state_size))
        #
        # # if it use zeros_like, we got error TypeError: 'numpy.int32' object does not support item assignment
        # Gt = np.zeros((episode_length, self.action_size))
        #
        # for i in range(episode_length):
        #     update_inputs[i] = self.states[i]
        #     Gt[i][self.actions[i]] = discounted_rewards[i]
        #
        # self.model.fit(update_inputs, Gt, epochs=1, verbose=0)
        # self.states, self.actions, self.rewards = [], [], []

if __name__ == '__main__':

    agent = REINFORCEAgent(env)
    scores, episodes = [], []

    # if os.path.exists("./save_model/cartpole_reinforce.h5"):
    #     print('load existing model')
    #     agent.model.load_weights("./save_model/cartpole_reinforce.h5")
    #     print('completed to load')

    start_time = timeit.default_timer()
    current_time = timeit.default_timer()

    for e in range(EPISODES):
        score = 0
        o = env.reset()
        # o = np.reshape(o, [1, agent.state_size])

        while True:
            if agent.render:
                env.render()

            a = agent.get_action(o)
            o, r, d, _ = env.step(a)
            # next_o = np.reshape(o, [1, agent.state_size])

            if is_training:
                r = r if not d or score == 499 else -100
                agent.append_sample(o, a, r)
                score += r
                agent.policy.rewards.append(r)

            if d:
                if is_training:
                    agent.train_model()

                score = score if score == 500 else score + 100
                scores.append(score)
                episodes.append(e)

                if is_training:
                    # exit
                    if np.mean(scores[-min(20, len(scores)):]) > 495:
                        pylab.plot(episodes, scores, 'b')
                        pylab.savefig("./save_graph/cartpole_reinforce_pytorch.png")
                        sys.exit()
                    break

        if is_training:
            # save the model
            if e % 200 == 0:
                pylab.plot(episodes, scores, 'b')
                pylab.savefig("./save_graph/cartpole_reinforce_pytorch.png")

                # agent.model.save_weights("./save_model/cartpole_reinforce.h5")
                current_time = timeit.default_timer()
                print('executed time for running 200 episodes : {}'.format(current_time-start_time))
                start_time = current_time


