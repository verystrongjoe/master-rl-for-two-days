"""
Trains an agent with (stochastic) Policy Gradients on Pong. Uses OpenAI Gym.
https://gist.github.com/karpathy/a4166c7fe253700972fcbc77e4ea32c5

10시 53분 부터 돌리기 시작함!!

resetting env. episode reward total was -11.000000. running mean: -11.988612
resetting env. episode reward total was -12.000000. running mean: -11.988726
time : Wed Oct 23 12:22:40 2019
resetting env. episode reward total was -16.000000. running mean: -12.028839
resetting env. episode reward total was -11.000000. running mean: -12.106405
"""

import numpy as np
import pickle
import gym
import time

# 하이퍼 파라메터 셋팅
H = 200  # 히든 레이어의 노드 수를 지정
batch_size = 10  # every how many episodes to do a param update?
learning_rate = 1e-4  # Gamma
gamma = 0.99  # Discounted Factor reward
decay_rate = 0.99  # decay factor for RMSProp leaky sum of grad^2
resume = False  # 저장한 모델을 이용해서 플레이를 지켜볼때 사용
render = False  # rendering은 학습 스피드를 빠르기 위해 꺼놓습니다.

# model initialization
D = 80 * 80  # input dimensionality: 80x80 grid

if resume:
    model = pickle.load(open('save.p', 'rb'))
else:
    model = {}
    model['W1'] = np.random.randn(H, D) / np.sqrt(D)  # "Xavier" initialization
    model['W2'] = np.random.randn(H) / np.sqrt(H)

# update buffers that add up gradients over a batch
grad_buffer = {k: np.zeros_like(v) for k, v in model.items()}

# RMSProp memory
rmsprop_cache = {k: np.zeros_like(v) for k, v in model.items()}

# 시그모이드
def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))  # sigmoid "squashing" function to interval [0,1]

"""
실제 게임 화면에서 필요없는 부분을 날리는 부분입니다.
경계면 삭제하고 paddle과 ball만 1로 마스킹처리하는 부분
이런걸 observation(환경에서 제공하는 것)을 state로 즉
에이전트가 쉽게 학습하기 위해 abundant information을 제거하는 부분입니다.
"""
def preprocess(I):
    """
    preprocess 210x160x3 uint8 frame into 6400 (80x80) 1D float vector
    """
    I = I[35:195]  # crop
    I = I[::2, ::2, 0]  # downsample by factor of 2
    I[I == 144] = 0  # erase background (background type 1)
    I[I == 109] = 0  # erase background (background type 2)
    I[I != 0] = 1  # everything else (paddles, ball) just set to 1
    return I.astype(np.float).ravel()


"""
이 부분은 바로 REINFORCE 알고리즘이 episodic하게 학습하기 때문에 
episode에서 얻은 샘플들을 한방에 return을 구해야하는데 효과적으로(incrementally update) 
하는 부분입니다. 실제로 지도학습이라면 이 부분이 X,Y의 데이터를 만드는 것이라고 보죠
정확하게는 training data를 직접 만들어내는 부분이라고 생각하시면 더 편하실 겁니다. 
"""
def discount_rewards(r):
    """ take 1D float array of rewards and compute discounted reward """
    discounted_r = np.zeros_like(r)
    running_add = 0
    for t in reversed(range(0, r.size)):
        # reset the sum, since this was a game boundary (pong specific!)
        if r[t] != 0 :
            running_add = 0
        running_add = running_add * gamma + r[t]
        discounted_r[t] = running_add
    return discounted_r


def policy_forward(x):
    """
    이부분은 바로 우리가 keras, pytorch, tensorflow를 쓰는 부분을
    단순하게 numpy로 표현하는 부분입니다. 위에서 정의한 numpy에 담긴 hidden node weight와
    저희가 전처리 해서 만든 state(observation)를 dot product 연산으로 신경망 연산을 이렇게
    쉽게 몇줄로 구현을 한 것입니다.
    """
    h = np.dot(model['W1'], x)
    # 정말 별게 없죠? 이 간단한게 신경망을 풍성하게 만든답니다. activation 함수의 역할이 중요한거죠
    h[h < 0] = 0  # ReLU nonlinearity
    logp = np.dot(model['W2'], h)
    p = sigmoid(logp)  # 마찬가지로 시그모이드라는 activation 함수입니다. 0에서 1의 값을 얻을때 사용합니다.
    return p, h  # return probability of taking action 2, and hidden state



"""
이 부분은 신경망이 학습되는 back propagation하는 부분입니다. 
보통 이를 backward pass라고 합니다. 실제로 tensor가 forward pass의 반대, 
즉 끝에서 시작해서 역으로 학습하기 때문에 이렇게 쉽게 부른답니다.
"""
def policy_backward(eph, epdlogp):
    """ backward pass. (eph is array of intermediate hidden states) """
    dW2 = np.dot(eph.T, epdlogp).ravel()
    dh = np.outer(epdlogp, model['W2'])
    dh[eph <= 0] = 0  # backpro prelu
    dW1 = np.dot(dh.T, epx)
    return {'W1': dW1, 'W2': dW2}

# Pong (탁구) 게임 객체를 만듭니다.
env = gym.make("Pong-v0")

# 초기화
observation = env.reset()
prev_x = None  # used in computing the difference frame
xs, hs, dlogps, drs = [], [], [], []
running_reward = None
reward_sum = 0
episode_number = 0

while True:

    # Pong(탁구게임)이 플레이 되는걸 보고 싶을때 ON 시키면 됩니다
    if render:
        env.render()

    # observation을 state로 전처리 하는 부분
    cur_x = preprocess(observation)
    # 이전 시점과 현재 시점의 화면 프레임의 비교하여 x에 저장한다.
    x = cur_x - prev_x if prev_x is not None else np.zeros(D)
    prev_x = cur_x # 현재시점을 과거 시점으로 저장해서 다음 스탭에 사용하기 위함

    # forward the policy network and sample an action from the returned probability
    aprob, h = policy_forward(x)
    action = 2 if np.random.uniform() < aprob else 3  # roll the dice!

    # record various intermediates (needed later for backprop)
    xs.append(x)  # observation
    hs.append(h)  # hidden state in first layer
    y = 1 if action == 2 else 0  # a "fake label"

    # grad that encourages the action that was taken to be taken
    # (see http://cs231n.github.io/neural-networks-2/#losses if confused)
    dlogps.append(y - aprob)

    # step the environment and get new measurements
    observation, reward, done, info = env.step(action)
    reward_sum += reward

    # record reward (has to be done after we call step() to get reward for previous action)
    drs.append(reward)

    if done:  # an episode finished
        episode_number += 1

        epx = np.vstack(xs)  # stacking all inputs for this episode
        eph = np.vstack(hs)  # stacking all  hidden states for this episode
        epdlogp = np.vstack(dlogps)  # stacking all action gradients for this episode
        epr = np.vstack(drs)  # stacking all rewards for this episode
        xs, hs, dlogps, drs = [], [], [], []  # reset array memory

        # compute the discounted reward backwards through time
        discounted_epr = discount_rewards(epr)

        # standardize the rewards to be unit normal (helps control the gradient estimator variance)
        discounted_epr -= np.mean(discounted_epr)
        discounted_epr /= np.std(discounted_epr)

        epdlogp *= discounted_epr  # modulate the gradient with advantage (PG magic happens right here.)
        grad = policy_backward(eph, epdlogp)
        for k in model: grad_buffer[k] += grad[k]  # accumulate grad over batch

        # perform rmsprop parameter update every batch_size episodes
        if episode_number % batch_size == 0:
            for k, v in model.items():
                g = grad_buffer[k]  # gradient
                rmsprop_cache[k] = decay_rate * rmsprop_cache[k] + (1 - decay_rate) * g ** 2
                model[k] += learning_rate * g / (np.sqrt(rmsprop_cache[k]) + 1e-5)
                grad_buffer[k] = np.zeros_like(v)  # reset batch gradient buffer

        # boring book-keeping
        running_reward = reward_sum if running_reward is None else running_reward * 0.99 + reward_sum * 0.01
        print('{} episode reward total was {}. running mean: {}'.format(
            episode_number, reward_sum, running_reward))

        if episode_number % 100 == 0:
            print(time.strftime('time : %c', time.localtime(time.time())))
            pickle.dump(model, open('save.p', 'wb'))

        reward_sum = 0
        observation = env.reset()  # reset env
        prev_x = None
