from common.utils import *
import os

params = {}

params[OPS.DISCOUNT_FACTOR.value] = [0.99]
params[OPS.BATCH_SIZE.value] = [32]
params[OPS.REPLAY_MEMORY_SIZE.value] = [100000]
params[OPS.LEARNING_RATE.value] = [0.001]
params[OPS.ENV_NAME.value] = ['BreakoutDeterministic-v4', 'PongNoFrameskip-v4']
params[OPS.N_STEPS.value] = [1000000]

params[OPS.EPSILON_START.value] = [1.0]
params[OPS.EPSILON_FINAL.value] = [0.01]
params[OPS.EPSILON_DECAY.value] = [30000]

params[OPS.BETA_START.value] = [0.4]
params[OPS.BETA_FRAMES.value] = [100000]
# 'pong_per.py', 'pong_categorical.py',

os.chdir('D:\\dev\\workspace\\temporary\\master-rl-for-two-days\\day2\\rainbow\\pong')

filenames = ['pong_per.py', 'pong_categorical.py','pong_noisy.py']

for filename in filenames:
    print("{} file started!!!!!".format(filename))
    auto_executor(params, os.path.realpath(filename))