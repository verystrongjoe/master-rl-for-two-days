""""

https://arxiv.org/pdf/1507.06527.pdf

12,000 episodes DRQN discovers a policy that allows it to reliably advance past the first level of Frostbite.
 --> 15000 에피소드
"""

import os

os.environ["CUDA_VISIBLE_DEVICES"]="1";
hiddenimports = ['pywt._extensions._cwt'],

# note  : pre-processing
from skimage.transform import resize
from skimage.color import rgb2gray

import numpy as np
import gym
import keras
from keras.models import Model
from keras.layers import TimeDistributed,Conv2D,LSTM,Dense,Flatten,Activation, Input, Reshape
from keras.optimizers import Adadelta, Adam

from rl.agents.dqn import DQNAgent
from rl.policy import BoltzmannQPolicy
from rl.memory import SequentialMemory
from rl.callbacks import TrainEpisodeLogger
from rl.callbacks import ModelIntervalCheckpoint
from rl2.callbacks import EpisodePlot
from rl2.util import *
from rl.core import Processor

from collections import *

ENV_NAME = 'Frostbite-v0'

# Get the environment and extract the number of actions.
env = gym.make(ENV_NAME)
np.random.seed(123)
env.seed(123)
nb_actions = env.action_space.n


def mmdd24hhmmss():
    return datetime.now().strftime('%m%d%H%M%S')


class ObservationProcessor(Processor):
    def process_observation(self, o):
        processed_observe = np.uint8(resize(rgb2gray(o), (84, 84), mode='constant') * 255)
        return processed_observe


# class ObservationProcessor(Processor):
#     def __init__(self, window=4):
#         self.history = deque(maxlen=4)
#         self.window = 4
#
#     # def process_observation(self, observation):
#     #     while len(self.history) < self.window:
#     #         self.history.append(observation)
#     #     return np.asarray(self.history)
#
#     def process_step(self, observation, reward, done, info):
#         if done:
#             self.history = []
#         observation = self.process_observation(observation)
#         reward = self.process_reward(reward)
#         info = self.process_info(info)
#         return observation, reward, done, info

processor = ObservationProcessor()

# Next, we build a very simple model.
## todo : build drqn
model = Model()
# inputs = Input((1, 8, 210, 160, 3,))
# h = Reshape((8, 210, 160, 3,))(inputs)
inputs = Input((4, 84, 84))
h = Reshape((4, 84, 84, 1))(inputs)
h = TimeDistributed(Conv2D(32, (8, 8), strides=(4, 4)))(h)
h = TimeDistributed(Conv2D(64, (4, 4), strides=(2, 2)))(h)
h = TimeDistributed(Conv2D(64, (3, 3), strides=(1, 1)))(h)
h = TimeDistributed(Flatten())(h)
h = LSTM(512)(h)
h = Dense(18)(h)
outputs = Dense(nb_actions, activation='linear')(h)
model = Model(inputs=inputs, outputs=outputs)

model.compile(optimizer=keras.optimizers.adadelta(lr=0.1, clipnorm=10.), loss='mse', metrics=['accuracy'])


# Finally, we configure and compile our agent. You can use every built-in Keras optimizer and
# even the metrics!
memory = SequentialMemory(limit=400000, window_length=4)
policy = BoltzmannQPolicy()


file_name = os.path.basename(__file__).split('.')[0]
# params = {'batchnorm':'no','mem':50000, 'policy':'boltzmann', 'nb_steps_warmup':'20'}
file_name_ts = file_name + '_' + '_' + mmdd24hhmmss() +'.png'

epi_plot_callback = EpisodePlot(filepath='{}.png'.format(file_name_ts), interval=200)

cbs = [epi_plot_callback]

dqn = DQNAgent(model=model, nb_actions=nb_actions, memory=memory, nb_steps_warmup=20,
               target_model_update=1e-2, policy=policy, enable_double_dqn=True, enable_dueling_network=True, processor=processor)
dqn.compile(Adam(lr=1e-3), metrics=['mae'])

# Okay, now it's time to learn something! We visualize the training here for show, but this
# slows down training quite a lot. You can always safely abort the training prematurely using
# Ctrl + C.
dqn.fit(env, nb_steps=10000000, visualize=False, verbose=2, callbacks=cbs, )

# After training is done, we save the final weights.
dqn.save_weights('dqn_{}_weights.h5f'.format(ENV_NAME), overwrite=True)

# Finally, evaluate our algorithm for 5 episodes.
dqn.test(env, nb_episodes=5, visualize=True)