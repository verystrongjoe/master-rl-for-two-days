"""
from  https://gist.github.com/karpathy/a4166c7fe253700972fcbc77e4ea32c5
"""

import numpy as np
import pickle
import gym

# Hyperparameters
H = 200
batch_size = 10
learning_rate = 1e-4
gamma = 0.99
decay_rate = 0.99
resume = False
render = False

# model initialization
D = 80 * 80  # input dimensionality : 80 x 80 grid
if resume:
    model = pickle.load(open('save.p', 'rb'))
else:
    model = {}
    model['W1'] = np.random.randn(H,D) / np.sqrt(D)
