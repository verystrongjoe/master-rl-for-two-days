"""
https://github.com/MorvanZhou/pytorch-A3C
https://github.com/dgriff777/rl_a3c_pytorch
https://www.topbots.com/survey-in-advanced-reinforcement-learning/
"""
import torch
import torch.multiprocessing as mp
import gym
import os
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import threading
import time
import pylab
from tensorboardX import SummaryWriter

torch.manual_seed(1234)
# os.environ['OMP_NUM_THREADS'] = '1'

scores = []
N_EPISODE = 2000
N_THREADS = 4
episode = 0
LEARNING_RATE = 0.001
SEED_DEFAULT = 123
MAX_GRAD_NORM = 5

writer = SummaryWriter()

# custom weights initialization called on netG and netD
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)


def set_init(layers):
    for layer in layers:
        nn.init.normal_(layer.weight, mean=0., std=0.1)
        nn.init.constant_(layer.bias, 0.)


class GlobalNetwork(nn.Module):

    def __init__(self, observation_space, action_space, env_name):
        super(GlobalNetwork, self).__init__()

        self.hidden1 = 24
        self.hidden2 = 24

        self.env_name = env_name
        self.linear1 = nn.Linear(observation_space, self.hidden1)
        # self.linear2 = nn.Linear(self.hidden1, self.hidden1)

        self.actor_hidden = nn.Linear(self.hidden1, self.hidden2)
        self.critic_hidden = nn.Linear(self.hidden1, self.hidden2)

        self.actor_linear = nn.Linear(self.hidden2, action_space)
        self.critic_linear = nn.Linear(self.hidden2, 1)  # value

        set_init(
            [self.linear1, self.actor_hidden,
                  self.critic_hidden, self.actor_linear, self.critic_linear]
                 )

    def forward(self, observation):
        shared = F.relu(self.linear1(observation))
        # shared = F.relu(self.linear2(shared))

        ah = F.relu(self.actor_hidden(shared))
        ch = F.relu(self.critic_hidden(shared))

        logits = self.actor_linear(ah)

        probs = F.softmax(logits, dim=-1)
        log_probs = F.log_softmax(logits, dim=-1)
        value = self.critic_linear(ch)

        # print('probs--> ', probs)
        # print('logits--> ', logits)

        return value, probs, log_probs
        # return value, logits


class LocalAgent(threading.Thread):
    def __init__(self, index, network, env_name, gamma, state_size, action_size):
        threading.Thread.__init__(self)

        self.states = []
        self.rewards = []
        self.actions = []

        self.log_probs = []
        self.entropies = []
        self.values = []

        self.index = index
        self.network = network
        self.env_name = env_name
        self.gamma = gamma

        self.action_size = action_size
        self.state_size = state_size

    def run(self):
        global episode

        # todo : 여기에서 seed값을 환경마다 다르게 해야함!!
        env = gym.make(self.env_name)
        # env.seed(SEED_DEFAULT + self.index)

        while episode < N_EPISODE:
            s = env.reset()
            s = torch.from_numpy(s).type(torch.FloatTensor)
            score = 0
            steps = 0

            values = []
            log_probs = []
            entropies = []
            rewards = []
            d = False

            while steps < 200:
                # value, logits, log_logits = self.network(s.unsqueeze(0))
                value, probs, log_probs_tensor = self.network(s.unsqueeze(0))
                # log_logits = F.log(logits + 1e-10)
                # print('value ---> ', value)

                entropy = (log_probs_tensor * probs).sum(1, keepdim=True)
                entropies.append(entropy)

                a = probs.multinomial(num_samples=1).detach()
                log_prob = log_probs_tensor.gather(1, a)

                ns, r, d, _ = env.step(a.numpy()[0][0])
                score += r
                ns = torch.from_numpy(ns).type(torch.FloatTensor)
                s = ns
                steps = steps + 1

                values.append(value)
                log_probs.append(log_prob)
                rewards.append(r)

                if d:
                    episode += 1
                    print('episode : {}, score : {}'.format(episode, score))
                    scores.append(score)
                    break

            R = torch.zeros(1, 1)
            # if not d:
            #     value, _, _ = self.network((s.unsqueeze(0)))
            #     R = value.detach()  # todo : question?

            values.append(R)

            policy_loss = 0
            value_loss = 0

            for t in reversed(range(0, len(rewards))):
                R = R * self.gamma + rewards[t]
                advantage = R - values[t]
                # print('R: {}, values[t]: {}, advantage : {}'.format(R.data, values[t].data, advantage.data))
                value_loss = value_loss + torch.pow(advantage, 2)
                policy_loss = policy_loss - (log_probs[t] * advantage.detach())
                policy_loss = policy_loss + 0.1 * entropies[t]

            value_loss = value_loss / len(rewards)
            policy_loss = policy_loss / len(rewards)

            # print('value_loss :{} , policy_loss  :{}'.format(value_loss, policy_loss))
            writer.add_scalar('data/value_loss', value_loss, episode)
            writer.add_scalar('data/policy_loss', policy_loss, episode)

            optimizer = optim.Adam(self.network.parameters(), lr=LEARNING_RATE)
            optimizer.zero_grad()

            (policy_loss + value_loss).backward()
            # torch.nn.utils.clip_grad_norm_(self.network.parameters(), MAX_GRAD_NORM)
            optimizer.step()

class Runner:
    def __init__(self, n_threads, env_name='PongDeterministic-v4'):
        self.env_name = env_name
        self.n_threads = n_threads

    def train(self):
        env = gym.make(self.env_name)
        network = GlobalNetwork(env.observation_space.shape[0], env.action_space.n, self.env_name)
        # network.apply(weights_init)


        agents = [LocalAgent(i, network, self.env_name, 0.99, env.observation_space.shape[0], env.action_space.n) for i in range(self.n_threads)]

        for agent in agents:
            agent.start()

        while True:
            time.sleep(30)
            plot = scores[:]
            # pylab.savefig("./save_graph/cartpole_a3c.png")


if __name__ == '__main__':
    runner = Runner(N_THREADS, env_name='CartPole-v0')
    runner.train()

    writer.export_scalars_to_json("./all_scalars.json")
    writer.close()
