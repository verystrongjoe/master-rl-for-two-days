"""
Reinforcement Learning (A3C) using Pytroch + multiprocessing.
The most simple implementation for continuous action.
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.multiprocessing as mp
from torch.optim import Adam
import gym
import os
import math
import numpy as np


UPDATE_GLOBAL_INTERVAL = 5
GAMMA = 0.99
MAX_EPISODE = 2000
N_HIDDEN = 200
MAX_EPISODE_STEP = 200


os.environ["OMP_NUM_THREADS"] = "1"

env = gym.make('Pendulum-v0')
n_obsevation_space = env.observation_space.shape[0]
n_action_space = env.action_space.shape[0]


def v_wrap(np_array, dtype=np.float32):
    if np_array.dtype != dtype:
        np_array = np_array.astype(dtype)
    return torch.from_numpy(np_array)


def push_and_pull(opt, lnet, gnet, done, s_, bs, ba, br, gamma):
    """
    :param opt:
    :param lnet: local network
    :param gnet: global network
    :param done: done
    :param s_: next state
    :param bs: backwrad state
    :param ba: backward action
    :param br: backward reward
    :param gamma: discount factor
    :return:
    """
    if done:
        v_s_ = 0. # terminaal
    else:
        v_s_ = lnet.forward(v_wrap(s_[None, :]))[-1].data.numpy()[0, 0] #todo

    buffer_v_target = []

    for r in br[::-1]: # reverse buffer r
        v_s_ = r + gamma * v_s_
        buffer_v_target.append(v_s_)
    buffer_v_target.reverse()

    loss = lnet.loss_func(
        v_wrap(np.vstack(bs)),
        v_wrap(np.array(ba), dtype=np.int64) if ba[0].dtype == np.int64 else v_wrap(np.vstack(ba)),
        v_wrap(np.array(buffer_v_target)[:, None]))

    opt.zero_grad()
    loss.backward()
    for lp, gp in zip(lnet.parameters(), gnet.parameters()):
        gp._grad = lp.grad  # ?
    opt.step()

    # synchronize global parameters
    lnet.load_state_dict(gnet.state_dict())

def record(global_ep, global_ep_r, ep_r, res_queue, name):
    with global_ep.get_lock():
        global_ep.value += 1

    with global_ep_r.get_lock():
        if global_ep_r.value == 0.:
            global_ep_r.value = ep_r
        else:
            global_ep_r.value = global_ep_r.value * 0.99 + ep_r * 0.01
        res_queue.put(global_ep_r.value)

        print(name, "Ep:", global_ep.value, "| Ep_r : %.0f" % global_ep_r.value)

def set_init(layers):
    for layer in layers:
        nn.init.normal_(layer.weight, mean=0., std=0.1)
        nn.init.constant_(layer.bias, 0.)


class Net(nn.Module):
    def __init__(self, s_dim, a_dim):
        super(Net, self).__init__()
        self.s_dim = s_dim
        self.a_dim = a_dim
        self.a1 = nn.Linear(s_dim, 200)
        self.mu = nn.Linear(200, a_dim)
        self.sigma = nn.Linear(200, a_dim)
        self.c1 = nn.Linear(s_dim, 100)
        self.v = nn.Linear(100, 1)
        set_init([self.a1, self.mu, self.sigma, self.c1, self.v])
        self.distribution = torch.distributions.Normal  # todo :

    def forward(self, x):
        a1 = F.relu6(self.a1(x))
        mu = 2 * F.tanh(self.mu(a1))  # todo :
        sigma = F.softplus(self.sigma(a1)) + 0.001      # avoid 0  # todo :
        c1 = F.relu6(self.c1(x))
        values = self.v(c1)
        return mu, sigma, values

    def choose_action(self, s):
        self.training = False
        mu, sigma, _ = self.forward(s)
        m = self.distribution(mu.view(1, ).data, sigma.view(1, ).data)
        return m.sample().numpy()

    def loss_func(self, s, a, v_t):
        self.train()
        mu, sigma, values = self.forward(s)
        td = v_t - values
        c_loss = td.pow(2)

        m = self.distribution(mu, sigma)
        log_prob = m.log_prob(a)
        entropy = 0.5 + 0.5 * math.log(2 * math.pi) + torch.log(m.scale)  # exploration
        exp_v = log_prob * td.detach() + 0.005 * entropy
        a_loss = -exp_v
        total_loss = (a_loss + c_loss).mean()
        return total_loss


class Worker(mp.Process):

    def __init__(self, gnet, opt, global_ep, global_ep_r, res_queue, name):
        super(Worker, self).__init__()
        self.name = 'w%i'% name # todo
        self.g_ep, self.g_ep_r, self.res_queue = global_ep, global_ep_r, res_queue
        self.gnet, self.opt = gnet, opt     # todo: what is opt?
        self.lnet = Net(n_obsevation_space, n_action_space) # create new local network
        self.env = gym.make('Pendulum-v0').unwrapped

    def run(self):

        total_step = 1
        while self.g_ep.value < MAX_EPISODE:
            s = self.env.reset()
            buffer_s, buffer_a, buffer_r = [],[],[]
            ep_r = 0.

            for t in range(MAX_EPISODE_STEP):
                if self.name == 'w0':
                    self.env.render()
                a = self.lnet.choose_action(v_wrap(s[None, :])) # todo : check this out s[None, :]

                # s_, r, done, _ = self.env.step(a)
                # if done:
                #     r = -1
                s_, r, done, _ = self.env.step(a.clip(-2, 2))
                if t == MAX_EPISODE_STEP - 1:
                    done = True

                ep_r += r

                buffer_a.append(a)
                buffer_s.append(s)
                # buffer_r.append(r)
                buffer_r.append((r + 8.1) / 8.1)  # normalize  --> 큰차이다!!

                if total_step % UPDATE_GLOBAL_INTERVAL == 0 or done:
                    # sync
                    push_and_pull(self.opt, self.lnet, self.gnet, done, s_, buffer_s, buffer_a, buffer_r, GAMMA)
                    buffer_s, buffer_a, buffer_r = [], [], []

                    if done: # done and print information
                        record(self.g_ep, self.g_ep_r, ep_r, self.res_queue, self.name)
                        break

                s = s_
                total_step += 1

        self.res_queue.put(None)

if __name__ == '__main__':

    gnet = Net(n_obsevation_space, n_action_space)  # global network
    gnet.share_memory()  # share global parameters in multiprocessing
    opt = Adam(gnet.parameters(), lr=0.0002)  # global optimizer
    global_ep, global_ep_r, res_queue = mp.Value('i', 0), mp.Value('d', 0.), mp.Queue()

    # parallel training
    workers = [Worker(gnet, opt, global_ep, global_ep_r, res_queue, i) for i in range(mp.cpu_count())]
    [w.start() for w in workers]

    res = []

    while True:
        r = res_queue.get()
        if r is not None:
            res.append(r)
        else:
            break

    [w.join() for w in workers]

    import matplotlib.pyplot as plt
    plt.plot(res)
    plt.ylabel('Moving average ep reward')
    plt.xlabel('Step')
    plt.show()



