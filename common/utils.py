import os
import itertools
import subprocess
import numpy as np
import pandas as pd
import sys
from enum import Enum
from datetime import datetime

# visualize
import matplotlib.pyplot as plt


def auto_executor(params, filename):
    '''

    사용법
    1. 사용하고 싶은 옵션을 OPS enum 클래스에 추가한다.
    2. params에 enum 의 값을 배열로 입력한다. ex : param[OPS.추가한옵션.value] = [실행할 변수 list]
    3. 수행할 filename 을 입력한다.
    4. 호출되는 소스에서 arg 처리를 해준다.

    import argparse
    from core.common.util import OPS

    parser = argparse.ArgumentParser(description='DQN Configuration including setting dqn / double dqn / double dueling dqn')

    parser.add_argument(OPS.NO_GUI.value, help='gui', type=bool, default=False)
    parser.add_argument(OPS.DOUBLE.value, help='double dqn', default=False, action='store_true')
    parser.add_argument(OPS.DUELING.value, help='dueling dqn', default=False, action='store_true')
    parser.add_argument(OPS.DRQN.value, help="drqn", default=False, action='store_true')
    parser.add_argument(OPS.BATCH_SIZE.value, type=int, default=128, help="batch size")
    parser.add_argument(OPS.REPLAY_MEMORY_SIZE.value, type=int, default=8000, help="replay memory size")
    parser.add_argument(OPS.LEARNING_RATE.value, type=float, default=0.001, help="learning rate")
    parser.add_argument(OPS.TARGET_NETWORK_UPDATE.value, type=int, default=60, help="target_network_update_interval")
    .
    .
    .
    parser.add_argument("추가한 옵션", type=타입, default=기본값, help="help 에 표시될 도움말" ...)

    args = parser.parse_args()

    dict_args = vars(args)
    post_fix = ''
    for k in dict_args.keys():
        if k == 'no_gui':
            continue
        post_fix += '_' + k + '_' + str(dict_args[k])


    5. args 를 적절히 소스에 사용해준다.
    6. output file naming 에 post_fix 를 추가해주면 좋음.


    '''
    # op = lambda k, l: np.concatenate([[k, v] if v is not None else [k] for v in l])
    op = lambda k, l: [[k, v] if v is not None else [k] for v in l]

    # 값이 배열로 주어지는 파라메터
    list_params = {}
    # T/F 로 주어지는 파라메터
    on_off_params = {}

    TOTAL_COUNT = 1

    while len(params):
        p = params.popitem()

        if p[1] == [None]:
            on_off_params[p[0]] = p[1]
            TOTAL_COUNT *= 2
        else:
            list_params[p[0]] = p[1]
            TOTAL_COUNT *= len(p[1])

    keys = on_off_params.keys()

    # on_off_params 이 모두 선택 안된 경우 실행
    ll = []

    for k in list_params.keys():
        ll.append(op(k, list_params[k]))

    now = 1

    for combination in itertools.product(*ll):
        if len(combination) != 0:
            print(np.concatenate(combination).tolist(), str(now) + '/' + str(TOTAL_COUNT))
            now += 1
            subargs = [sys.executable, filename] + np.concatenate(combination).tolist()
            subprocess.call(subargs, shell=True)

    # on_off_params 가 하나 이상 선택된 경우 실행
    for s in range(len(on_off_params)):
        for k_l in itertools.combinations(keys, s + 1):
            ll = []
            for k in k_l:
                ll.append(op(k, on_off_params[k]))

            for k in list_params.keys():
                ll.append(op(k, list_params[k]))

            for combination in itertools.product(*ll):
                if len(combination) != 0:
                    print(np.concatenate(combination).tolist(), str(now) + '/' + str(TOTAL_COUNT))
                    now += 1
                    subargs = [sys.executable, filename] + np.concatenate(combination).tolist()
                    subprocess.call(subargs, shell=True)


class OPS(Enum):

    # warn : please make value as short as possible. because if those values are added, it could exceed 255 length. (windows file max length : 255)

    # Common for agent
    N_STEPS = '-nsteps'
    ACTION_REPETITION = '-act-rep'
    DOUBLE = '-double'
    DUELING = '-dueling'
    BATCH_SIZE = '-batch-size'
    TARGET_NETWORK_UPDATE_INTERVAL = '-intv-tn-up'
    REPLAY_MEMORY_SIZE = '-rem-size'
    LEARNING_RATE = '-lr'
    DISCOUNT_FACTOR = '-gamma'

    EPSILON_START = '-eps-start'
    EPSILON_FINAL = '-eps-final'
    EPSILON_DECAY = '-eps-decay'

    BETA_START = '-beta-start'
    BETA_FRAMES = '-beta-frames'

    # For Actor Critic Architecture
    LEARNING_CRITIC_RATE = '-critic-lr'
    LEARNING_ACTOR_RATE = '-actor-lr'

    # DDPG Exploration
    OU_SIGMA = '-ou-sm'
    OU_THETA = '-ou-tt'
    USE_PARAMETERIZED_NOISE = '-p-noise'
    PURE_ACTION_RATIO = '-ratio-pure-action'

    ENV_NAME = '-env-nm'

    def __call__(self):
        return self._value_[1:].replace('-', '_')


def create_post_fix(dict_args):
    post_fix = ''
    for k in dict_args.keys():
        if k == 'no_gui':
            continue
        post_fix += '_' + k + '_' + str(dict_args[k])

    print('post_fix : {}'.format(post_fix))
    return post_fix

def mmdd24hhmmss():
    return datetime.now().strftime('%m%d%H%M%S')

def yyyymmdd24hhmmss():
    return datetime.now().strftime('%Y%m%d%H%M%S')

def display_param_list(params=[]):
    return '_'.join(params)

def display_param_dic(params={}):
    lc = []
    for key, value in zip(params.keys(), params.values()):
        lc.append(str(key) + "_" + str(value))
    return '_'.join(lc)

def get_additional_log_info(f):
    CURRENT_FILE_NAME = os.path.basename(f).split('.')[0]
    CURRENT_FILE_PATH = os.path.sep.join(os.path.abspath(f).split(os.path.sep)[:-1])
    FILE_NAME_FOR_LOG = os.path.basename(f).split('.')[0] + "_" + yyyymmdd24hhmmss() + '_test'

    return CURRENT_FILE_NAME, CURRENT_FILE_PATH, FILE_NAME_FOR_LOG


def plot(rewards, losses, filename):
    plt.figure(figsize=(20,5))
    plt.subplot(131)
    plt.title('reward: %s' % (np.mean(rewards[-10:])))
    plt.plot(rewards)
    plt.subplot(132)
    plt.title('loss')
    plt.plot(losses)
    plot_file = filename + ".png"
    plt.savefig(plot_file)
    print('plot figure saved into {}.'.format(plot_file))
    plt.show()




