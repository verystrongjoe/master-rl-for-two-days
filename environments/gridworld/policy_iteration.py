from rlGridworld import GridWorld


if __name__ == '__main__':

    env = GridWorld(mode='policy_iteration')
    print('start grid world problem for policy iteration...')

    env.loop()