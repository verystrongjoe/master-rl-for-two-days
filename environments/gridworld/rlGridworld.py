"""
Grid World Environment made by DMQA RL Study Group
"""
import numpy as np
import pygame
from gym.core import Env as Env
from config import *
import random

class GridWorld(Env):

    def __init__(self, mode='human'):
        """
        처음에 에이전트가 생성될 경우 최초 한번 호출되는 부분이며, pygame 인스턴스를 초기화하고 환경에 처음 정의되어야 하는부분을 다 기술
        :param mode: 'policy_iteration, value_iteration, q_learning, sarsa, monte_carlo
        """
        pygame.init()

        self.grid_total_row_size = ENV.GRID_CELL_SIZE * ENV.GRID_ROW_COUNT
        self.grid_total_col_size = ENV.GRID_CELL_SIZE * ENV.GRID_COL_COUNT
        assert self.grid_total_row_size  < ENV.MAX_WORLD_ROW_PIXEL_SIZE
        assert self.grid_total_col_size < ENV.MAX_WORLD_ROW_PIXEL_SIZE

        """
        시각화 관련 정보들 초기화
        """
        self.screen = pygame.display.set_mode([ENV.GRID_CELL_SIZE * ENV.GRID_ROW_COUNT, ENV.GRID_CELL_SIZE * ENV.GRID_COL_COUNT + 100])  # 100 = EXTRA_SIZE_FOR_BUTTON

        self.font = pygame.font.SysFont('consolas', ENV.FONT_SIZE, 1)
        pygame.display.set_caption(ENV.CAPTION_NAME)

        # action value, state value, reward, policy table
        self.action_value_table = np.zeros((ENV.GRID_COL_COUNT, ENV.GRID_ROW_COUNT, ENV.ACTION_SIZE))
        self.state_value_table = np.zeros((ENV.GRID_COL_COUNT, ENV.GRID_ROW_COUNT))
        self.reward_table = np.zeros((ENV.GRID_COL_COUNT, ENV.GRID_ROW_COUNT))
        self.policy_table = np.zeros((ENV.GRID_COL_COUNT, ENV.GRID_ROW_COUNT))
        self.discounted_factor_reward = AGENT.DISCOUNT_FACTOR_REWARD
        self.learning_rate = AGENT.LEARNING_RATE
        self.epsilon = AGENT.EPSILON

        """
        정책 관련하여 시각화 컴포넌트 준비 
        policy left, right, up, down arrow
        """
        up_img = pygame.image.load(r'up.png')
        down_img = pygame.image.load(r'down.png')
        left_img = pygame.image.load(r'left.png')
        right_img = pygame.image.load(r'right.png')

        image_size = (int(ENV.GRID_CELL_SIZE/6), int(ENV.GRID_CELL_SIZE/6))
        self.up_img = pygame.transform.scale(up_img, image_size)
        self.down_img = pygame.transform.scale(down_img, image_size)
        self.left_img = pygame.transform.scale(left_img, image_size)
        self.right_img = pygame.transform.scale(right_img, image_size)
        

        """
        환경관련
        """
        self.action_size = ENV.ACTION_SIZE

        """
        에이전트, 골, 장애물에 대한 위치 관련 정보
        """
        self.agent_pos = ENV.AGENT_POS
        self.list_obs_pos = ENV.LIST_OBSTACLE_POS
        assert len(self.list_obs_pos) == ENV.OBSTACLE_COUNT
        self.goal_pos = ENV.GOAL_POS
        self.grid_cell_size = ENV.GRID_CELL_SIZE
        self.grid_row_count = ENV.GRID_ROW_COUNT
        self.grid_col_count = ENV.GRID_COL_COUNT

        """
        리워드 정보 셋팅
        """
        self.goal_reward = REWARD.GOAL_REWARD
        self.obs_reward = REWARD.OBS_REWARD
        self.invalid_action_rewared = REWARD.INVALID_ACTION_REWARD
        self.mode = mode


        """
        몬테카를로용 episodic buffer
        """
        self.episode_buffer = []


        """
        버튼 박스 4개 만들기 20, 190, 360, 530
        """
        self.button_1 = pygame.Rect(30, self.grid_total_col_size + self.grid_cell_size/4, self.grid_cell_size*(4/5), self.grid_cell_size / 4)
        self.button_2 = pygame.Rect(30 + self.grid_cell_size*1, self.grid_total_col_size + self.grid_cell_size/4, self.grid_cell_size*(4/5), self.grid_cell_size / 4)
        self.button_3 = pygame.Rect(30 + self.grid_cell_size*2, self.grid_total_col_size + self.grid_cell_size/4, self.grid_cell_size*(4/5), self.grid_cell_size / 4)
        self.button_4 = pygame.Rect(30 + self.grid_cell_size*3, self.grid_total_col_size + self.grid_cell_size/4, self.grid_cell_size*(4/5), self.grid_cell_size / 4)
        
        """
        states 정보
        """
        self.states = []
        self.possible_actions = [0, 1, 2, 3]  # 상, 하, 좌, 우

        # 리워드 설정
        self.reward_table[self.goal_pos[0]][self.goal_pos[1]] = REWARD.GOAL_REWARD

        for o in self.list_obs_pos:
            self.reward_table[o[0]][o[1]] = REWARD.OBS_REWARD

        for x in range(self.grid_row_count):
            for y in range(self.grid_col_count):
                s = [x,y]
                self.states.append(s)

        """
        보통 가장 init하단에 reset을 깔끔하게 호출!!
        """
        self.reset()


    def printText(self, msg, pos, color='BLACK'):
        textSurface = self.font.render(msg, True, pygame.Color(color), None)
        textRect = textSurface.get_rect()
        textRect.topleft = pos
        self.screen.blit(textSurface, textRect)

    def reset(self):
        """
        에피소드가 종료되고 초기화되어야하는 부분. 생성자에서 초기화되는 부분과 유사되나 여긴 값을 초기화한다는 점에서 차별이 됨
        그래서 보통 생성자함수에서 인스턴스 정의하고 이 함수를 호출하여 값 초기화하는 부분의 코드 중복을 피한다.
        :return:
        """
        self.done = False
        self.clock = pygame.time.Clock()
        self.agent_pos = ENV.AGENT_STARTING_POS.copy()

        if self.mode == 'value_iteration':
            # 가치함수를 2차원 리스트로 초기화
            self.state_value_table = [[0.0] * self.grid_row_count for _ in range(self.grid_col_count)]

        if self.mode == 'policy_iteration' or self.mode == 'value_iteration':
            # 상 하 좌 우 동일한 확률로 정책 초기화
            self.action_value_table = self.action_value_table + 0.25
            # 마침 상태의 설정
            self.action_value_table[self.goal_pos[0]][self.goal_pos[1]] = [0.] * 4

        return self.agent_pos

    def step(self, action):
        """
        액션을 강화학습 에이전트가 아닌 인간의 키보드 입력을 통해서 받게 하는 부분
        """
        if self.mode == 'human':

            # env.clock.tick(10)  # todo : tune this value

            # https://stackoverflow.com/questions/25494726/how-to-use-pygame-keydown
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_DOWN]:
                # print('down')
                if self.agent_pos[1] + 1 < self.grid_row_count:
                    self.agent_pos[1] += 1
                # else:
                    # print('impossible to move')

            elif pressed[pygame.K_UP]:
                # print('up')
                if self.agent_pos[1] - 1 >= 0:
                    self.agent_pos[1] -= 1
                # else:
                #     print('impossible to move')

            elif pressed[pygame.K_LEFT]:
                # print('left')
                if self.agent_pos[0] - 1 >= 0:
                    self.agent_pos[0] -= 1
                # else:
                #     print('impossible to move')

            elif pressed[pygame.K_RIGHT]:
                # print('right')
                if self.agent_pos[0] + 1 < self.grid_col_count:
                    self.agent_pos[0] += 1
                # else:
                #     print('impossible to move')

            if self.agent_pos == self.goal_pos or self.agent_pos in self.list_obs_pos:
                print('done')
                self.done = True
        else:
            """
            강화학습 에이전트 통해서 액션을 받게 하는 부분
            """
            # Main Event Loop
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    done = True  # Flag that we are done so we exit this loop

            # https://stackoverflow.com/questions/25494726/how-to-use-pygame-keydown
            # [0, 1, 2, 3]  # 상, 하, 좌, 우
            if action == 'down' or action == 1:
                # print('down')
                if self.agent_pos[1] + 1 < self.grid_row_count:
                    self.agent_pos[1] += 1
                # else:
                #     print('impossible to move')

            elif action == 'up' or action == 0:
                # print('up')
                if self.agent_pos[1] - 1 >= 0:
                    self.agent_pos[1] -= 1
                # else:
                #     print('impossible to move')

            elif action == 'left' or action == 2:
                # print('left')
                if self.agent_pos[0] - 1 >= 0:
                    self.agent_pos[0] -= 1
                # else:
                #     print('impossible to move')

            elif action  == 'right' or action == 3:
                # print('right')

                if self.agent_pos[0] + 1 < self.grid_col_count:
                    self.agent_pos[0] += 1
                # else:
                #     print('impossible to move')

            if self.agent_pos == self.goal_pos or self.agent_pos in self.list_obs_pos:
                # print('done')
                self.done = True

            next_state = self.agent_pos.copy()
            reward = self.get_reward(next_state)
            return next_state, reward, self.done

    def state_after_action(self, state, action):

        # assert self.mode == 'policy_iteration' or self.mode == 'value_iteration'

        # [0, 1, 2, 3]  # 상, 하, 좌, 우

        next_state = state.copy()

        if action == 1:
            # print('down')
            if state[1] + 1 < self.grid_row_count:
                next_state[1] += 1

        elif action == 0:
            # print('up')
            if state[1] - 1 >= 0:
                next_state[1] -= 1

        elif action == 2:
            # print('left')
            if state[0] - 1 >= 0:
                next_state[0] -= 1

        elif action == 3:
            # print('right')
            if state[0] + 1 < self.grid_col_count:
                next_state[0] += 1

        # if next_state == self.goal_pos:
            # print('done')
            # self.done = True

        return next_state

    def get_reward(self, next_state):
        # assert self.mode == 'policy_iteration' or self.mode == 'value_iteration'
        return self.reward_table[next_state[0]][next_state[1]]


    def render(self, mode='human'):
        """
        :param mode: ignored.
        :return:
        """
        self.screen.fill(GUI.WHITE)
        # grid lines drawing
        for x in range(self.grid_col_count + 1):
            pygame.draw.line(self.screen, GUI.BLACK, (0, x * self.grid_cell_size), (self.grid_total_col_size, x * self.grid_cell_size))
        for y in range(self.grid_row_count):
            pygame.draw.line(self.screen, GUI.BLACK, (y * self.grid_cell_size, 0), (y * self.grid_cell_size, self.grid_total_col_size))

        # agent drawing
        x_of_agent = self.agent_pos[0] * self.grid_cell_size + self.grid_cell_size / 4
        y_of_agent = self.agent_pos[1] * self.grid_cell_size + self.grid_cell_size / 4
        pygame.draw.rect(self.screen, GUI.RED, [x_of_agent, y_of_agent, self.grid_cell_size / 2, self.grid_cell_size / 2])

        # obstacle drawing (triangle)
        for o in self.list_obs_pos:
            x_of_obs = o[0] * self.grid_cell_size
            y_of_obs = o[1] * self.grid_cell_size

            # get three coordinates for triangle
            p1_obs = x_of_obs + self.grid_cell_size / 2, y_of_obs + self.grid_cell_size / 4
            p2_obs = x_of_obs + self.grid_cell_size / 5, y_of_obs + self.grid_cell_size * 4 / 5
            p3_obs = x_of_obs + self.grid_cell_size * 4 / 5, y_of_obs + self.grid_cell_size * 4 / 5

            pygame.draw.polygon(self.screen, GUI.GREEN, [p1_obs, p2_obs, p3_obs], 0)

        # goal drawing (circle)
        center_of_circle = (
            int(self.goal_pos[0] * self.grid_cell_size + self.grid_cell_size / 2),
            int(self.goal_pos[1] * self.grid_cell_size + self.grid_cell_size / 2)
        )

        pygame.draw.circle(self.screen, GUI.BLUE, center_of_circle, int(self.grid_cell_size / 4))

        margin = 10

        for y in range(self.grid_row_count):
            for x in range(self.grid_col_count):
                for a in range(self.action_size):  # [0, 1, 2, 3]  # 상(0), 하(1), 좌(2), 우(3)
                    if a == 2:  # left
                        pos = (x * self.grid_cell_size + self.grid_cell_size / 3 * 0 + + self.grid_cell_size / 3 / 4 - margin,
                         y * self.grid_cell_size + self.grid_cell_size / 3 * 1 + self.grid_cell_size / 3 / 4)
                        if self.mode == 'policy_iteration' or self.mode == 'value_iteration':
                            if self.goal_pos != [x, y] and self.action_value_table[x][y][2] > 0.:
                                self.screen.blit(self.left_img, pos)
                        elif self.mode == 'q_learning' or self.mode == 'sarsa':
                            self.printText(str(self.action_value_table[x][y][a]), pos)

                    elif a == 3:  # right
                        pos = (x * self.grid_cell_size + self.grid_cell_size/3 * 2 + + self.grid_cell_size / 3 / 4  + margin,
                                                             y * self.grid_cell_size + self.grid_cell_size/3 * 1 + self.grid_cell_size / 3 / 4)
                        if self.mode == 'policy_iteration' or self.mode == 'value_iteration':
                            if self.goal_pos != [x, y] and self.action_value_table[x][y][3] > 0.:
                                self.screen.blit(self.right_img, pos)
                        elif self.mode == 'q_learning' or self.mode == 'sarsa':
                            self.printText(str(self.action_value_table[x][y][a]), pos)

                    elif a == 0:  # up
                        pos = (x * self.grid_cell_size + self.grid_cell_size / 3 * 1 + + self.grid_cell_size / 3 / 4,
                                             y * self.grid_cell_size + self.grid_cell_size / 3 * 0 + self.grid_cell_size / 3 / 4 - margin)
                        if self.mode == 'policy_iteration' or self.mode == 'value_iteration':
                            if self.goal_pos != [x, y] and self.action_value_table[x][y][0] > 0.:
                                self.screen.blit(self.up_img, pos)  # paint to screen
                        elif self.mode == 'q_learning' or self.mode == 'sarsa':
                            self.printText(str(self.action_value_table[x][y][a]), pos)

                    elif a == 1:  # down
                        pos = (x * self.grid_cell_size + self.grid_cell_size / 3 * 1 + + self.grid_cell_size / 3 / 4,
                                             y * self.grid_cell_size + self.grid_cell_size /3 * 2 + self.grid_cell_size / 3 / 4 + margin)
                        if self.mode == 'policy_iteration' or self.mode == 'value_iteration':
                            if self.goal_pos != [x, y] and self.action_value_table[x][y][1] > 0.:
                                self.screen.blit(self.down_img, pos)  # paint to screen

                        elif self.mode == 'q_learning' or self.mode == 'sarsa':
                            self.printText(str(self.action_value_table[x][y][a]), pos)

        for y in range(self.grid_row_count):
            for x in range(self.grid_col_count):
                pos = (x * self.grid_cell_size + self.grid_cell_size / 3 * 2 + + self.grid_cell_size / 3 / 4,
                       y * self.grid_cell_size + self.grid_cell_size / 3 * 2 + self.grid_cell_size / 3 / 4)
                if self.mode == 'policy_iteration' or self.mode == 'value_iteration' or self.mode =='monte_carlo':
                    self.printText(str(self.state_value_table[x][y]), pos)
        """
        button 
        """
        if self.mode == 'policy_iteration' or self.mode == 'value_iteration':
            button_names = []
            if self.mode == 'policy_iteration':
                button_names = ['Evaluate', 'Improve', 'Move', 'Reset']
            else:
                button_names = ['Calculate', 'Policy', 'Move', 'Clear']
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_1)
            self.printText(button_names[0], (self.button_1.left + 5, self.button_1.top + 10))
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_2)
            self.printText(button_names[1], (self.button_2.left + 5, self.button_2.top + 10))
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_3)
            self.printText(button_names[2], (self.button_3.left + 5, self.button_3.top + 10))
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_4)
            self.printText(button_names[3], (self.button_4.left + 5, self.button_4.top + 10))
        elif self.mode == 'q_learning':
            button_names = ['Roll out', 'Auto Play']
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_1)
            self.printText(button_names[0], (self.button_1.left + 5, self.button_1.top + 10))
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_2)
            self.printText(button_names[1], (self.button_2.left + 5, self.button_2.top + 10))

        elif self.mode == 'sarsa':
            button_names = ['Roll out', 'Auto Play']
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_1)
            self.printText(button_names[0], (self.button_1.left + 5, self.button_1.top + 10))
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_2)
            self.printText(button_names[1], (self.button_2.left + 5, self.button_2.top + 10))

        elif self.mode == 'monte_carlo':
            button_names = ['Roll out', 'Auto Play']
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_1)
            self.printText(button_names[0], (self.button_1.left + 5, self.button_1.top + 10))
            pygame.draw.rect(self.screen, GUI.GRAY, self.button_2)
            self.printText(button_names[1], (self.button_2.left + 5, self.button_2.top + 10))

        pygame.display.flip()

    def close(self):
        # Be IDLE friendly
        pygame.quit()

    def seed(self, seed=None):
        pass

    def get_all_states(self):
        return self.states

    def loop(self):

        while True:
            self.render()

            # Main Event Loop
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    done = True  # Flag that we are done so we exit this loop
                # https://stackoverflow.com/questions/47639826/pygame-button-single-click
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    # 1 is left mouse button
                    if event.button == 1:
                        # 'event.pos' is the mouse position.
                        if self.button_1.collidepoint(event.pos):
                            print('button_1')
                            if self.mode == 'policy_iteration':
                                self.policy_evaluation()
                            elif self.mode == 'value_iteration':
                                self.value_iteration()
                            elif self.mode == 'q_learning':
                                self.q_learning_roll_out()
                            elif self.mode == 'sarsa':
                                self.sarsa_roll_out()
                            elif self.mode == 'monte_carlo':
                                self.mc_roll_out()
                        elif self.button_2.collidepoint(event.pos):
                            print('button_2')
                            if self.mode == 'policy_iteration':
                                self.policy_improvement()
                            elif self.mode == 'value_iteration':
                                self.print_optimal_policy()
                            elif self.mode == 'q_learning':
                                self.q_learning_roll_out_auto()
                            elif self.mode == 'sarsa':
                                self.sarsa_roll_out_auto()
                            elif self.mode == 'monte_carlo':
                                self.mc_roll_out_auto()
                        elif self.button_3.collidepoint(event.pos):
                            print('button_3')
                            self.move_by_policy()
                        elif self.button_4.collidepoint(event.pos):
                            print('button_4')
                            self.reset()

    def policy_evaluation(self):
        # 다음 가치함수 초기화
        next_value_table = [[0.00] * self.grid_row_count for _ in range(self.grid_col_count)]

        # 모든 상태에 대해서 벨만 기대방정식을 계산
        for state in self.get_all_states():
            value = 0.0
            # 마침 상태의 가치 함수 = 0
            if state == self.goal_pos:
                next_value_table[state[0]][state[1]] = value
                continue

            # 벨만 기대 방정식
            for action in self.possible_actions:
                next_state = self.state_after_action(state, action)

                reward = 0

                if state == next_state:
                    reward = REWARD.INVALID_ACTION_REWARD

                else:
                    reward = self.get_reward(next_state)

                next_value = self.get_value(next_state)
                value += (self.get_policy(state)[action] *
                          (reward + self.discounted_factor_reward * next_value))

            next_value_table[state[0]][state[1]] = round(value, 2)

        self.state_value_table = next_value_table

    # 현재 가치 함수에 대해서 탐욕 정책 발전
    def policy_improvement(self):
        next_policy = self.action_value_table
        for state in self.get_all_states():
            if state == self.goal_pos:
                continue
            value = -99999
            max_index = []
            # 반환할 정책 초기화
            result = [0.0, 0.0, 0.0, 0.0]

            # 모든 행동에 대해서 [보상 + (감가율 * 다음 상태 가치함수)] 계산
            for index, action in enumerate(self.possible_actions):
                next_state = self.state_after_action(state, action)
                reward = self.get_reward(next_state)
                next_value = self.get_value(next_state)
                temp = reward + self.discounted_factor_reward * next_value

                # 받을 보상이 최대인 행동의 index(최대가 복수라면 모두)를 추출
                if temp == value:
                    max_index.append(index)
                elif temp > value:
                    value = temp
                    max_index.clear()
                    max_index.append(index)

            # 행동의 확률 계산
            prob = 1 / len(max_index)

            for index in max_index:
                result[index] = prob

            next_policy[state[0]][state[1]] = result

        self.action_value_table = next_policy

    # 현재 가치 함수로부터 행동을 반환
    def get_actions(self, state):
        action_list = []
        max_value = -99999

        if state == self.goal_pos:
            return []

        # 모든 행동에 대해 큐함수 (보상 + (감가율 * 다음 상태 가치함수))를 계산
        # 최대 큐 함수를 가진 행동(복수일 경우 여러 개)을 반환
        for action in self.possible_actions:

            next_state = self.state_after_action(state, action)
            reward = self.get_reward(next_state)
            next_value = self.get_value(next_state)
            value = (reward + self.discounted_factor_reward * next_value)

            if value > max_value:
                action_list.clear()
                action_list.append(action)
                max_value = value
            elif value == max_value:
                action_list.append(action)

        return action_list

    # 가치 이터레이션
    # 벨만 최적 방정식을 통해 다음 가치 함수 계산
    def value_iteration(self):
        next_state_value_table = [[0.0] * self.grid_row_count for _ in range(self.grid_col_count)]
        for state in self.get_all_states():
            if state == self.goal_pos:
                next_state_value_table[state[0]][state[1]] = 0.0
                continue

            # 가치 함수를 위한 빈 리스트
            value_list = []

            # 가능한 모든 행동에 대해 계산
            for action in self.possible_actions:
                next_state = self.state_after_action(state, action)
                reward = self.get_reward(next_state)
                next_value = self.get_value(next_state)
                value_list.append((reward + self.discounted_factor_reward * next_value))

            # 최댓값을 다음 가치 함수로 대입 (차이점!!!)
            next_state_value_table[state[0]][state[1]] = round(max(value_list), 2)

        self.state_value_table = next_state_value_table

    def print_optimal_policy(self):
        for state in self.get_all_states():
            # goal state은 policy 없는 terminal state
            if state == self.goal_pos:
                continue

            x = state[0]
            y = state[1]

            print(x,y)

            for a in self.possible_actions:
                self.action_value_table[x][y][a] = 0

            max_actions = self.get_actions(state)

            for a in max_actions:  # [0, 1, 2, 3]  # 상, 하, 좌, 우
                self.action_value_table[x][y][a] = 1 / len(max_actions)

    def move_by_policy(self):
        # if self.improvement_count != 0 and self.is_moving != 1:
        self.is_moving = 1
        x, y = self.agent_pos
        step = 0

        while len(self.action_value_table[x][y]) != 0 and not self.done and step < ENV.MAX_STEP:
            action = self.get_action([x, y])
            print(action)
            self.step(action)
            print(self.agent_pos)
            self.clock.tick(ENV.TICK_INTERVAL)
            self.render()
            step += 1
            x, y = self.agent_pos
        self.is_moving = 0

    # 특정 상태에서 정책에 따른 행동을 반환
    def get_action(self, state):
        return np.random.choice(ENV.ACTION_SIZE, 1, p=self.action_value_table[state[0]][state[1]])[0]

    # 상태에 따른 정책 반환
    def get_policy(self, state):
        if state == self.goal_pos:
            return [0.0] * len(self.possible_actions)
        return self.action_value_table[state[0]][state[1]]

    # 가치 함수의 값을 반환
    def get_value(self, state):
        # 소숫점 둘째 자리까지만 계산
        return round(self.state_value_table[state[0]][state[1]], 2)

    # 큐함수에 의거하여 입실론 탐욕 정책에 따라서 행동을 반환
    def get_epsilon_greedy_action(self, state):
        if np.random.rand() > self.epsilon:
            # 무작위 행동 반환
            action = np.random.choice(self.possible_actions)
        else:
            # 큐함수에 따른 행동 반환
            state_action = self.action_value_table[state[0]][state[1]]
            action = self.arg_max(state_action)
        return action

    @staticmethod
    def arg_max(state_action):
        max_index_list = []
        max_value = state_action[0]
        for index, value in enumerate(state_action):
            if value > max_value:
                max_index_list.clear()
                max_value = value
                max_index_list.append(index)
            elif value == max_value:
                max_index_list.append(index)
        return random.choice(max_index_list)

    # <s, a, r, s'> 샘플로부터 큐함수 업데이트
    def train_q(self, state, action, reward, next_state):

        q_1 = self.action_value_table[state[0]][state[1]][action]

        # 벨만 최적 방정식을 사용한 큐함수의 업데이트 # [0, 1, 2, 3]  # 상, 하, 좌, 우
        q_2 = reward + self.discounted_factor_reward * max(self.action_value_table[next_state[0]][next_state[1]])

        self.action_value_table[state[0]][state[1]][action] = \
            round(self.action_value_table[state[0]][state[1]][action] + self.learning_rate * (q_2 - q_1), 2)

    # <s, a, r, s', a'>의 샘플로부터 큐함수를 업데이트
    def train_s(self, state, action, reward, next_state, next_action):
        current_q = self.action_value_table[state[0]][state[1]][action]
        next_state_q = self.action_value_table[next_state[0]][next_state[1]][next_action]
        new_q = (current_q + self.learning_rate *
                (reward + self.discounted_factor_reward * next_state_q - current_q))
        self.action_value_table[state[0]][state[1]][action] = round(new_q, 2)

    # 메모리에 샘플을 추가
    def save_sample(self, state, reward, done):
        self.episode_buffer.append([state, reward, done])


    def mc_roll_out(self):
        state = self.reset()
        action = self.get_epsilon_greedy_action(state)

        while True:
            self.render()

            # 다음 상태로 이동 보상은 숫자이고, 완료 여부는 boolean
            next_state, reward, done = self.step(action)
            self.save_sample(next_state, reward, done)

            # 다음 행동 받아옴
            action = self.get_epsilon_greedy_action(next_state)

            # 에피소드가 완료됐을 때, 큐 함수 업데이트
            if done:
                self.update()
                self.episode_buffer.clear()
                break

    def mc_roll_out_auto(self):
        for episode in range(1000):
            self.mc_roll_out()

    # 모든 에피소드에서 에이전트가 방문한 상태의 큐 함수를 업데이트
    def update(self):
        G_t = 0
        visit_state = []
        for s in reversed(self.episode_buffer):
            key_s = str(s[0])
            if key_s not in visit_state:
                visit_state.append(key_s)
                G_t = s[1] + self.discounted_factor_reward * G_t
                value = self.state_value_table[s[0][0]][s[0][1]]
                self.state_value_table[s[0][0]][s[0][1]] = round(value + self.learning_rate * (G_t - value), 2)

    def q_learning_roll_out(self):

        state = self.reset()

        while True:
            self.render()
            self.clock.tick(ENV.TICK_INTERVAL)

            # 현재 상태에 대한 행동 선택 # [0, 1, 2, 3]  # 상, 하, 좌, 우
            action = self.get_epsilon_greedy_action(state)
            # 행동을 취한 후 다음 상태, 보상 에피소드의 종료여부를 받아옴
            next_state, reward, done = self.step(action)

            # <s,a,r,s'>로 큐함수를 업데이트
            self.train_q(state, action, reward, next_state)
            state = next_state

            if done:
                break
        self.render()


    def q_learning_roll_out_auto(self):
        state = self.reset()
        episode = 0

        while episode <= ENV.AUTO_TOTAL_EPISODES:
            episode += 1
            print('{} episode'.format(episode))
            self.q_learning_roll_out()

    def sarsa_roll_out(self):

        # 게임 환경과 상태를 초기화
        state = self.reset()

        # 현재 상태에 대한 행동을 선택
        action = self.get_epsilon_greedy_action(state)

        while True:
            self.render()
            self.clock.tick(ENV.TICK_INTERVAL)

            # 행동을 위한 후 다음상태 보상 에피소드의 종료 여부를 받아옴
            next_state, reward, done = self.step(action)
            # 다음 상태에서의 다음 행동 선택
            next_action = self.get_epsilon_greedy_action(next_state)

            # <s,a,r,s'>로 큐함수를 업데이트
            self.train_s(state, action, reward, next_state, next_action)

            state = next_state
            action = next_action

            if done:
                break
        self.render()

    def sarsa_roll_out_auto(self):
        state = self.reset()
        episode = 0

        while episode <= ENV.AUTO_TOTAL_EPISODES:
            episode += 1
            print('{} episode'.format(episode))
            self.sarsa_roll_out()