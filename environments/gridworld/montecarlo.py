from rlGridworld import GridWorld


# 메인 함수
if __name__ == "__main__":

    env = GridWorld(mode='monte_carlo')
    print('start grid world problem for monte carlo...')

    env.loop()
