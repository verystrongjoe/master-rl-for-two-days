# environment
class GUI:
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    GRAY = (128, 128, 128)


class ENV:
    FONT_SIZE = 15
    GRID_ROW_COUNT = 5
    GRID_COL_COUNT = 5
    GRID_CELL_SIZE = 100
    OBSTACLE_COUNT = 2
    MAX_WORLD_ROW_PIXEL_SIZE = 800
    MAX_WORLD_COL_PIXEL_SIZE = 850
    GOAL_POS = [2, 2]  # type must be list
    AGENT_STARTING_POS = [0, 0]
    AGENT_POS = [0, 0] # type must be list
    LIST_OBSTACLE_POS = [[1, 2], [2, 1]]
    CAPTION_NAME = 'Flexible Grid World by Uk Jo'
    ACTION_SIZE = 4
    MAX_STEP = 100
    TICK_INTERVAL = 50  # the smaller it is, the slower the game plays
    AUTO_TOTAL_EPISODES = 1000

class REWARD:
    INVALID_ACTION_REWARD = 0
    GOAL_REWARD = 100.0
    OBS_REWARD = -100.0

class AGENT:
    ENABLE_RANDOM_STARING_POS_AGENT = False
    DISCOUNT_FACTOR_REWARD = 0.9
    LEARNING_RATE = 0.01
    EPSILON = 0.9





