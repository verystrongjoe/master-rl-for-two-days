import pygame
import numpy as np

pygame.init()

# parameters for preparing environment
OBSTACLE_CNT = 2
GRID_SIZE = [5, 5]
GRID_CELL_SIZE = 150
GRID_PIXEL_SIZE = [GRID_SIZE[0] * GRID_CELL_SIZE, GRID_SIZE[1] * GRID_CELL_SIZE]
GOAL_POS = [2, 2]
AGENT_STARTING_POS = [0, 0]

AGENT_POS = AGENT_STARTING_POS.copy()

OBSTACLE_STARTING_POS_LIST = [(1, 1), (1, 3)]

screen = pygame.display.set_mode([GRID_PIXEL_SIZE[0], GRID_PIXEL_SIZE[1]+100])  # 100 = En!@77XTRA_SIZE_FOR_BUTTON

font = pygame.font.SysFont('consolas', 15, 0)
pygame.display.set_caption('gridworld by uk')

done = False
clock = pygame.time.Clock()


def printText(msg, pos, color='BLACK'):
    textSurface = font.render(msg, True, pygame.Color(color), None)
    textRect = textSurface.get_rect()
    textRect.topleft = pos
    screen.blit(textSurface, textRect)


"""
action value tabular data
"""
ACTION_SIZE = 4
action_value_table = np.zeros((GRID_SIZE[0], GRID_SIZE[1], ACTION_SIZE))


"""
policy left, right, up, down arrow
"""
up_img = pygame.image.load(r'up.png')
down_img = pygame.image.load(r'down.png')
left_img = pygame.image.load(r'left.png')
right_img = pygame.image.load(r'right.png')

up_img = pygame.transform.scale(up_img, (20, 20))
down_img = pygame.transform.scale(down_img, (20, 20))
left_img = pygame.transform.scale(left_img, (20, 20))
right_img = pygame.transform.scale(right_img, (20, 20))


if __name__ == '__main__':

    while not done:
        clock.tick(10)

        # Main Event Loop
        for event in pygame.event.get():  # User did something

            if event.type == pygame.QUIT:  # If user clicked close
                done = True  # Flag that we are done so we exit this loop

            # https://stackoverflow.com/questions/25494726/how-to-use-pygame-keydown
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_DOWN]:
                print('down')

                if AGENT_POS[1] + 1 < GRID_SIZE[0]:
                    AGENT_POS[1] += 1
                else:
                    print('impossible to move')

            elif pressed[pygame.K_UP]:
                print('up')

                if AGENT_POS[1] - 1 >= 0:
                    AGENT_POS[1]-=1
                else:
                    print('impossible to move')

            elif pressed[pygame.K_LEFT]:
                print('left')

                if AGENT_POS[0]-1 >= 0:
                    AGENT_POS[0]-=1
                else:
                    print('impossible to move')

            elif pressed[pygame.K_RIGHT]:
                print('right')

                if AGENT_POS[0]+1 < GRID_SIZE[1]:
                    AGENT_POS[0]+=1
                else:
                    print('impossible to move')

            if AGENT_POS == GOAL_POS:
                print('done')
                done = True

        screen.fill(WHITE)

        # grid lines drawing
        for x in range(GRID_SIZE[1]+1):
            pygame.draw.line(screen, BLACK, (0, x * GRID_CELL_SIZE), (GRID_PIXEL_SIZE[1], x*GRID_CELL_SIZE))
        for y in range(GRID_SIZE[0]):
            pygame.draw.line(screen, BLACK, (y*GRID_CELL_SIZE, 0), (y*GRID_CELL_SIZE, GRID_PIXEL_SIZE[1]))

        # agent drawing
        x_of_agent = AGENT_POS[0] * GRID_CELL_SIZE + GRID_CELL_SIZE/4
        y_of_agent = AGENT_POS[1] * GRID_CELL_SIZE + GRID_CELL_SIZE/4

        pygame.draw.rect(screen, RED, [x_of_agent, y_of_agent, GRID_CELL_SIZE/2, GRID_CELL_SIZE/2])

        # obstacle drawing
        for o in OBSTACLE_STARTING_POS_LIST:

            x_of_obs = o[1] * GRID_CELL_SIZE
            y_of_obs = o[0] * GRID_CELL_SIZE

            # get three coordinates for triangle
            p1_obs = x_of_obs + GRID_CELL_SIZE / 2, y_of_obs + GRID_CELL_SIZE / 4
            p2_obs = x_of_obs + GRID_CELL_SIZE / 5, y_of_obs + GRID_CELL_SIZE * 4/5
            p3_obs = x_of_obs + GRID_CELL_SIZE * 4/5, y_of_obs + GRID_CELL_SIZE * 4/5

            pygame.draw.polygon(screen, BLUE, [p1_obs, p2_obs, p3_obs], 0)

        # goal drawing
        center_of_circle = (int(GOAL_POS[0]*GRID_CELL_SIZE+ GRID_CELL_SIZE/2), int(GOAL_POS[1]*GRID_CELL_SIZE+ GRID_CELL_SIZE/2))
        pygame.draw.circle(screen, GREEN, center_of_circle, int(GRID_CELL_SIZE/4))


        for y in range(GRID_SIZE[0]):
            for x in range(GRID_SIZE[1]):
                for a in range(ACTION_SIZE):
                    # 0 : left, 1 : right, 2 :up, 3:down
                    if a == 0: #left
                        v_left_pos = (x * GRID_CELL_SIZE + GRID_CELL_SIZE * 1/15, y * GRID_CELL_SIZE + GRID_CELL_SIZE / 2 -20)
                        printText(str(action_value_table[y][x][a]), v_left_pos)


                        screen.blit(left_img, (x * GRID_CELL_SIZE + GRID_CELL_SIZE * 1/15, y * GRID_CELL_SIZE + GRID_CELL_SIZE / 2 + 20 -20) )

                    elif a == 1: # right
                        v_right_pos = (x * GRID_CELL_SIZE + GRID_CELL_SIZE * 5/6 - GRID_CELL_SIZE * 1/15, y * GRID_CELL_SIZE + GRID_CELL_SIZE /2 -20)
                        printText(str(action_value_table[y][x][a]), v_right_pos)

                        screen.blit(right_img, (x * GRID_CELL_SIZE + GRID_CELL_SIZE * 5/6 - GRID_CELL_SIZE * 1/15,
                                                y * GRID_CELL_SIZE + GRID_CELL_SIZE /2 + 20 -20))

                    elif a == 2: # up
                        v_up_pos = (x * GRID_CELL_SIZE + GRID_CELL_SIZE / 2, y * GRID_CELL_SIZE + GRID_CELL_SIZE * 1/8)
                        printText(str(action_value_table[y][x][a]), v_up_pos)

                        screen.blit(up_img, (x * GRID_CELL_SIZE + GRID_CELL_SIZE / 2 -25,
                                             y * GRID_CELL_SIZE + GRID_CELL_SIZE * 1/8))  # paint to screen

                    elif a ==3: #down
                        v_down_pos = (x * GRID_CELL_SIZE + GRID_CELL_SIZE /2, y * GRID_CELL_SIZE + GRID_CELL_SIZE * 5/6)
                        printText(str(action_value_table[y][x][a]), v_down_pos)

                        screen.blit(down_img, (x * GRID_CELL_SIZE + GRID_CELL_SIZE /2 -25,
                                             y * GRID_CELL_SIZE + GRID_CELL_SIZE * 5/6))  # paint to screen

        # button
        pygame.draw.rect(screen, GRAY, [20, 780, GRID_CELL_SIZE, GRID_CELL_SIZE/4])
        printText('buton1', (70, 790))

        # button
        pygame.draw.rect(screen, GRAY, [190, 780, GRID_CELL_SIZE, GRID_CELL_SIZE / 4])
        printText('buton2', (240, 790))

        # button
        pygame.draw.rect(screen, GRAY, [360, 780, GRID_CELL_SIZE, GRID_CELL_SIZE / 4])
        printText('buton3', (410, 790))

        # button
        pygame.draw.rect(screen, GRAY, [530, 780, GRID_CELL_SIZE, GRID_CELL_SIZE / 4])
        printText('buton4', (580, 790))

        pygame.display.flip()


    # Be IDLE friendly
    pygame.quit()

