from rlGridworld import GridWorld


if __name__ == '__main__':

    env = GridWorld(mode='sarsa')
    print('start grid world problem for policy iteration...')

    env.loop()