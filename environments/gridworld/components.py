class Component(object):

    def __init__(self, name, colour, x, y, width, height, label):
        self.colour = colour
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.label = label


class Button(Component):
    def __init__(self, name, colour, x, y, width, height, label, status):
        super(Button, self).__init__()
        self.status = status  # 1 : pushed 1 : normal

    def isPushed(self):
        return self.status

