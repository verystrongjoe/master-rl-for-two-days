from rlGridworld import GridWorld


if __name__ == '__main__':

    env = GridWorld(mode='value_iteration')
    print('start grid world problem for value iteration...')

    env.loop()