"""
# reference
https://github.com/lilianweng/multi-armed-bandit
"""

# use this problem to introudct a number of basic learning method which we
# extend in later chapters to apply to thee full reinforcement learning problem.
# when the bandit problem become associative, that is, when action are taken in more than one situations.

from __future__ import division

import time
import numpy as np


class Bandit(object):

    def generate_reward(self, i):
        raise NotImplementedError


class BernoulliBandit(Bandit):

    def __init__(self, n, probas=None):
        assert probas is None or len(probas) == n
        self.n = n

        if probas is None:
            np.random.seed(int(time.time()))  # random seed
            self.probas = [np.random.random() for _ in range(self.n)]  # set 0 ~ 1 probability for each bandit
        else:
            self.probas = probas # if it is already set, use the one already set

        self.best_proba = max(self.probas)  # set the best one's probability

    def generate_reward(self, i):
        # The player selected the i-th machine.
        if np.random.random() < self.probas[i]:
            return 1   # win
        else:
            return 0   # lose