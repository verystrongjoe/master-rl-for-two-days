import tensorflow as tf
import tensorflow.keras.layers as kl
from tensorflow.keras.models import Model
from tensorflow.python.keras.optimizers import Adam

import gym
from collections import deque
import numpy as np
import random

from tensorboard.plugins.hparams import api as hp # 텐서보드에서 hyperparam tuning을 위한 api 임포트!!
import collections

# 아래는 하이퍼파라메터 조합군을 만들기 위한 각 하이퍼파라메터들의 값후보들을 정의하는 부분
HP_MINIBATCH_SIZE = hp.HParam('minibatch_size', hp.Discrete([64, 32]))
HP_REPLAY_MEMORY_SIZE = hp.HParam('replay_memory_size', hp.Discrete([2000, 4000]))
HP_DISCOUNTED_FACTOR = hp.HParam('discounted_factor', hp.Discrete([0.99, 0.999]))
HP_LEARNING_RATE = hp.HParam('learning_rate', hp.Discrete([0.001, 0.005]))
METRIC_ACCURACY = 'rewards'

# 텐서보드를 시각화 데이터 만드는 파일을 저장할 폴더를 지정하고 metric 및 hparams 설정
with tf.summary.create_file_writer('logs/hparam_tuning').as_default():
    hp.hparams_config(
        hparams=[HP_MINIBATCH_SIZE, HP_REPLAY_MEMORY_SIZE, HP_DISCOUNTED_FACTOR, HP_LEARNING_RATE],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


# Hyperparameters for DQN
minibatch_size = 64        # hparam
replay_memory_size = 2000  # hparam
discounted_factor = 0.99   # hparam
learning_rate = 0.001      # hparam

# epsilon greedy algorithm parameter
initial_exploration = 1.
final_exploration = 0.01
final_exploration_frame = 100000

"""
Agent for play
"""
class Agent:

    def __init__(self, state_size, action_size, replay_memory_size):
        self.replay_memory = deque([], maxlen=replay_memory_size)
        # self.model = model
        self.n_steps = 0
        self.n_epsiodes = 0
        self.epsilon = initial_exploration
        self.epsilon_decay = (initial_exploration - final_exploration) / final_exploration_frame
        self.epsilon_decay = 0.999
        self.state_size = state_size
        self.action_size = action_size
        self.discounted_factor = discounted_factor

        # https://www.tensorflow.org/versions/r2.0/api_docs/python/tf/keras/models/clone_model
        # self.target_model = tf.keras.models.clone_model(self.model)
        # self.target_model = DeepQNetwork(state_size=state_size, action_size=action_size)

        self.model= build_network(state_size, action_size)
        self.target_model = build_network(state_size, action_size)
        self.update_target()

    def update_target(self):
        self.target_model.set_weights(self.model.get_weights())

    def reset(self):
        self.replay_memory.clear()

    def select_action(self, s):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        else:
            value = self.model.predict(np.expand_dims(s, axis=0))[0]
            return np.argmax(value)

    def append_sample(self, state, action, reward, next_state, done):
        self.replay_memory.append(([state, action, reward, next_state, done]))
        if self.epsilon > final_exploration:
            self.epsilon *= self.epsilon_decay

    def sample(self, minibatch_size):
        return random.sample(self.replay_memory, minibatch_size)

    def train(self):
        if len(self.replay_memory) < minibatch_size:
            return

        samples = self.sample(minibatch_size)

        states = []
        actions = []
        rewards = []
        next_states = []
        dones = []

        for s, a, r, ns, d in samples:
            states.append(s)
            actions.append(a)
            rewards.append(r)
            next_states.append(ns)
            dones.append(d)

        q_values = self.model.predict(np.vstack(states), batch_size=minibatch_size)
        target_values = self.target_model.predict(np.vstack(next_states), batch_size=minibatch_size)

        for i in range(len(samples)):
            if not dones[i]:
                y = rewards[i] + self.discounted_factor * np.amax(target_values[i])
            else:
                y = rewards[i]
            q_values[i][actions[i]] = y

        self.model.fit(x=np.stack(states), y=q_values, epochs=1, verbose=0, batch_size=minibatch_size)

def build_network(state_size, action_size):

    input = tf.keras.Input(shape=(state_size,))
    h = kl.Dense(24, activation='relu',kernel_initializer='he_uniform')(input)
    h = kl.Dense(24, activation='relu',kernel_initializer='he_uniform')(h)
    output = kl.Dense(action_size, activation='linear',kernel_initializer='he_uniform')(h)
    model = Model(inputs=input, outputs=output)

    model.summary()
    model.compile(loss='mse', optimizer=Adam(lr=learning_rate))

    return model


# if __name__ == "__main__"

session_num = 0

for h_m in HP_MINIBATCH_SIZE.domain.values:
    for h_r in HP_REPLAY_MEMORY_SIZE.domain.values:
        for h_d in HP_DISCOUNTED_FACTOR.domain.values:
            for h_l in HP_LEARNING_RATE.domain.values:
                hparams = {
                    HP_MINIBATCH_SIZE: h_m,
                    HP_REPLAY_MEMORY_SIZE: h_r,
                    HP_DISCOUNTED_FACTOR : h_d,
                    HP_LEARNING_RATE : h_l
                }
                run_name = "run-%d" % session_num
                print('--- Starting trial: %s' % run_name)
                print({h.name: hparams[h] for h in hparams})

                with tf.summary.create_file_writer('logs/hparam_tuning/' + run_name).as_default():
                    hp.hparams(hparams)  # record the values used in this trial

                    minibatch_size = hparams[HP_MINIBATCH_SIZE]
                    replay_memory_size = hparams[HP_REPLAY_MEMORY_SIZE]
                    discounted_factor = hparams[HP_DISCOUNTED_FACTOR]
                    learning_rate = hparams[HP_LEARNING_RATE]

                    # agent play with hparams!
                    EPISODES = 80
                    TOTAL_STEP = 0

                    env = gym.make('CartPole-v0')
                    state_size = env.observation_space.shape[0]
                    action_size = env.action_space.n
                    n_max_step = env._max_episode_steps

                    print('============ Environment Information ============')
                    print('state size : {}, action size : {}, max steps : {}'.format(state_size, action_size, n_max_step))

                    s = env.reset()
                    agent = Agent(state_size, action_size, replay_memory_size)

                    last_n_rewards = collections.deque(maxlen=10)  # 마지막 10개의 리워드 저장

                    # episode loop
                    for e in range(EPISODES):  # loop until 500
                        """episode start"""
                        o = env.reset()
                        R = 0
                        d = False
                        step = 0

                        # inside episode
                        while not d:

                            # 만약에 동작하는 CartPole 모습을 보고 싶으면 여기 주석을 해제하시면 됩니다.
                            # env.render()

                            TOTAL_STEP += 1
                            step += 1

                            action = agent.select_action(o)
                            next_o, r, d, _ = env.step(action)

                            if d and step < n_max_step - 1:
                                r = -100

                            agent.append_sample(o, action, r, next_o, d)
                            agent.train()

                            R += r
                            o = next_o

                            if d:
                                agent.update_target()

                                if step < n_max_step - 1:
                                    R += 100

                                last_n_rewards.append(R)  # add item
                                print('{} episode, total steps : {}, return : {}'.format(e, TOTAL_STEP, R))
                                break

                    tf.summary.scalar(METRIC_ACCURACY, np.mean(last_n_rewards), step=1)
                session_num += 1
