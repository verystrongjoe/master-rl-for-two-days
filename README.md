# master-rl-for-two-days
Lecture slides and source codes reinforcement learning for 2 days 


## Background knowledge
강화학습 기본/실전편 사전 지식

### 1. 통계&확률
고등학교 수준의 통계/확률 지식이 필요하며 간단히 확률분포부터 기대값등에 대한 정의만 알아도 충분합니다.  
레퍼런스를 요청하신다면, 간단히 그것도 만화로 설명하는 얇은 책이 있어요,  
"만화 통계 7일 만에 끝내기"라는 책 같은걸로 한번 훑으시면 충분할것 같습니다.

### 2. 파이선과 Numpy
요즘 유행하는 언어 파이선은 코드 자체가 직관적이고 배우기 쉽지만, 처음 접하는 분은 분명히 자바와 C 랭기지와는
다른 컨벤션이나 문법등으로 핸즈온 시간에 어려움이 있을 수 있을겁니다. 
물론 간단하게 Numpy를 다루긴 하겠지만 거기에 시간을 너무 할애하기엔 강의 시간이 짧네요.

간단한 Hello world와 특히 Numpy를 예제를 좀 다뤄보고 와야합니다. 레퍼런스가 워낙 많으니 인터넷에 예제코드 정도 
튜토리얼 따라하면 될 것 같습니다. 적어도 눈에 익히는 차원에서입니다. 그래도 추천하자면,

점프투 파이선이라고 온라인ebook인데 (오프라인으로 출간) 강추합니다.
https://wikidocs.net/book/1

### 3. 머신러닝/딥러닝  
이 부분은 책이 많이 있으나 간단한 뉴럴넷 (CNN정도)까지의 개념의 이해가 필요하며 아래의 레퍼런스가 있겠습니다. 

모두의 딥러닝 유투브 채널을 출퇴근시간에 보면 처음엔 큰도움이 되실 겁니다. 
https://www.youtube.com/watch?v=BS6O0zOGX4E&list=PLlMkM4tgfjnLSOjrEJN31gZATbcj_MpUm


### 4. 강화학습  
기초부터 들어가지만 어려운 부분 없어 막힘 없이 워밍업으로 볼 수 있는 유투브 채널이 있습니다. 제 강좌는 Richard Sutton 교수님의

Reinforcement Learning이라는 책을 가지고 강의를 진행하며 확실히 Fundemental을 다루는 부분이라 먼저 숲을 보고 재미를 붙이실려면, 

김성훈 교수님의 모두의 RL 강좌를 추천합니다.

https://www.youtube.com/playlist?list=PLlMkM4tgfjnKsCWav-Z2F-MMFRx-2gMGG



## installation
  - 파이선 Python 3.7.4  설치
    https://www.python.org/downloads/release/python-374/
  - Git 설치 (64-bit Git for Windows Setup)
    https://git-scm.com/downloads
  - 실습 레포지토리 클론
    https://github.com/verystrongjoe/master-rl-for-two-days
    https://github.com/verystrongjoe/master-rl-for-two-days.git

    pip install virtualenv
    C:\>workspace\python>virtualenv ProjectEnv( here venv)
    C:\>workspace\python>cd ProjectEnv\Scripts
    C:\>workspace\python\ProjectEnv\Scripts>activate

  - pip install -r requirement.txt 실행
    https://www.tensorflow.org/install
  - 테스트 돌려보는 스크립트를 만들어볼수 있을려나...
  - check tf.20.py를 실행한다
  - tensorboard를 확인한다. (http://localhost:6006/)
  - mnist & dqn 소스를 돌려본다
  - bandit.py도 돌려본다  
  - cartpole-dqn.py도 돌려본다.

## 실습환경 체크 (19.07.30) 
  - Python 3.7.3 실제 교육장소에 설치된것으로 이걸 테스트하겠음
  - 실습환경 
    . CPU i7-7700 3.60GHZ, 16GB, 64Bit Windows 10 Pro
    . 그래픽 카드 GPU GT 730 2Gb 
     . 384core, 2gb https://www.geforce.com/hardware/desktop-gpus/geforce-gt-730/specifications
     . https://www.reddit.com/r/deeplearning/comments/70wxtd/is_a_geforce_gt_730_suitable_for_a_beginner/
     . https://hiseon.me/data-analytics/tensorflow/tensorflow-requirements/
      . 텐스플로우의 GPU 가속의 기능을 이용하기 위해서 최소 하드웨어 사양으로는 GeForece 750 Ti를 추천해 드립니다.
     . 그냥 CPU로!! 

## Reference
  - Richard Sutton second complete version
    http://incompleteideas.net/book/the-book-2nd.html
  -   
  
## Git issue
아래와 같은 커맨드 날리면 git id/password를 두번 물어보는 일 제거 가능


$ git config --system --unset credential.helper

